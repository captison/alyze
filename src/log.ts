class Log
{
    testMsg: string = '[%e] was caught for test method %m - "false" was returned.';

    out: () => any = () => {};

    // info(...args: any[]): void { this.log('[ INFO ]', args); }

    warn(...args: any[]): void { this.log('[ WARN ]', args); }

    // fail(...args: any[]): void { this.log('[FAILED]', args); }

    test(test: () => boolean, error: Error): void
    {
        this.warn(this.testMsg.replace('%m', test.prototype._name_).replace('%e', error + ''));
    }

    log(level: string, args: any[]): void { this.out.apply(null, [level, 'alyze:'].concat(args)); }

    set(out: () => any) { if (typeof out === 'function') this.out = out; }
}

export var log = new Log();
