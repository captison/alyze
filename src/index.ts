import { log } from './log';
import * as Main from './main/validator';
import * as Utils from './main/utils';


// application configuration interface
export interface AppConfig extends Main.Config
{
    extend?: Object;
    log?: () => any;
}
// default configuration
let defaultConfig: Main.Config = {};
// global configuration
export function config(config: AppConfig = {}): any
{
    log.set(config.log);
    // add any specified extensions
    if (config.extend) Main.extend(config.extend);
    // set default onMissingValue
    if (typeof config.onMissingValue === 'boolean') defaultConfig.onMissingValue = config.onMissingValue;
    // set default asMissing
    if (config.asMissing) defaultConfig.asMissing = config.asMissing

    return this;
}
// create new validator instance and return
export function create(config: Main.Config = {}): Main.Validator
{
    return new Main.Validator(Utils.merge({}, defaultConfig, config));
}
