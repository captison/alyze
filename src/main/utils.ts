import * as Const from './constants'
import * as Types from './types'

/*
    Converts the first character of `value` to upper case.

    @param {string} value
        The value to convert to upper case.
    @return {string}
        The capitalized string.
 */
export function capitalize(value: string): string
{
    return value.slice(0, 1).toUpperCase() + value.slice(1);
}

export function descriptor(object: Object, property: string): any
{
    return Object.getOwnPropertyDescriptor(object, property) || {};
}

export function ensure(value: any, def: any): any
{
    return typeof value === 'undefined' || value === null ? def : value;
}

export function escapeRE(value: string): string
{
    return value.replace(Const.re.escapeChars, "\\$&");
}

export function find(item: any, path: string = ''): any
{
    if (path.length > 0 && typeof item === 'object')
    {
        let index = path.indexOf('.');
        return index >= 0 ? find(item[path.slice(0, index)], path.slice(index + 1)) : find(item[path]);
    }
    else if (path.length === 0)
    {
        return item;
    }
}

/**
    Loops through the owned properties of `object` calling `func` with the
    value and key for each entry.

    @param {object} object
        The object to be iterated.
    @param {function} func
        The callback for each iteration.
    @return {array}
        Keys of `object`.
 */
export function loop(object: Object, func?: (v: any, k: string) => any): Array<string>
{
    let array: Array<string> = [];

    for (let key in object || {})
    {
        if (object.hasOwnProperty(key))
        {
            array.push(key);
            if (func) func(object[key], key);
        }
    }

    return array;
}

/**
    Checks `value` against the regular expression in the following way:
        - if count is undefined, is entire `value` a repetitive match
          against `re`? (i.e. /^(re)+$/)
        - if count is more than 0, does `value` have at least `count`
          matches for 're'? (i.e. /re/g)
        - if count is less than 0, does `value` have at most `count`
          matches for 're'? (i.e. /re/g)
        - otherwise, does `value` have zero matches for 're'? (i.e. /re/g)

    @param {*} value
        Test value.
    @param {string} re
        Regular expression to test against.
    @param {number} [count]
        Minimal number of times the regular expression must match.
    @return {boolean}
        True if the match check passes.
 */
export function matchRepeats(value: any, re: string, count?: number): boolean
{
    if (typeof count === 'undefined' || count === null)
        return new RegExp('^(' + re + ')+$').test(value);
    else if (count < 0)
        return matchCount(value, new RegExp(re, 'g')) >= Math.abs(count);
    else
        return matchCount(value, new RegExp(re, 'g')) === count;
}

/**
    Returns the number of times `regex` matches `value`.

    @return {number}
        The match count.
 */
export function matchCount(value: any, regex: RegExp): number
{
    return (String(value).match(regex) || []).length;
}

/**
    Copies all properties from `objects` into `target` object in the order
    given. If a value for a property is undefined then it is skipped.

    @param {object} target
        The merging target. A new object is created if not specified.
    @param {...object} objects
        The object(s) to be merged. Null items are skipped.
    @return {array}
        The target object.
 */
export function merge(target: Object, ...objects: Object[]): Object
{
    target = target || {};

    for (let object of objects)
    {
        loop(object, (v: any, k: string) => { if (typeof v !== 'undefined') target[k] = v; });
    }

    return target;
}

/**
    Checks `value` for inclusion of `search`.

    If `value` is a string, then 'search' is coerced to string and an
    .indexOf() test is performed.

    If `value` is an array, then `search` is coerced into an array and
    the sequence of values in `search` is looked for in `value`.

    If `value` is not an array or string then an array containing only `value`
    is checked against `search`.

    @param {any} value
        The value to be checked.
    @param {any} search
        The value to check for.
    @param {boolean} reverse
        Reverse `value` and `search` before comparison (check from end)?
    @return
        The index of `search` in `value`.
 */
export function indexOf(value: any, search: any, reverse: boolean): number
{
    if (Object.prototype.toString.call(value) === Types.stype.string)
    {
        if (reverse)
            return value.split('').reverse().join('').indexOf(String(search).split('').reverse().join(''));
        else
            return value.indexOf(search);
    }
    else if (Array.isArray(value))
    {
        var vItem = reverse ? [].concat(value).reverse() : value;
        var sItem = reverse ? [].concat(search).reverse() : [].concat(search);
        // as all strings contain the empty string, so must all arrays...
        if (sItem.length === 0) return 0;

        var idx = vItem.indexOf(sItem[0]);

        while (idx >= 0)
        {
            var matched = true, segment = vItem.slice(idx, idx + sItem.length);
            // check value segment for match against search
            for (var i=0,imx=sItem.length;i<imx;i++)
            {
                if(!(matched = matched && segment[i] === sItem[i]))
                    break;
            }

            if (matched)
                break;
            else
                idx = vItem.indexOf(sItem[0], idx + 1);
        }

        return idx;
    }
    else
    {
        return [value].indexOf(search);
    }
}

/**
    Returns a set of values derived from `path` in the `objects` array.

    @param {array} objects
        The array of objects having the values to obtain.
    @param {string} path
        The path in `objects` to obtain values from.
    @return {array}
        Set of unique values from the `path` in `objects`.
 */
export function toSet(objects: Array<Object>, path: string): Array<any>
{
    let array: Array<any> = [];

    for (let object of objects)
    {
        let value = find(object, path);
        if (typeof value !== 'undefined' && array.indexOf(value) < 0)
            array.push(value);
    }

    return array;
}
