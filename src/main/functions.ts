import * as Utils from './utils'
import * as Const from './constants'
import * as datatypes from './types'


export let fn: any = {}

/******************************************************************************
    Type (typeof) Methods
******************************************************************************/
fn.type_$flags = { }

fn.type_boolean = (v: any): boolean => typeof v === datatypes.type.boolean

fn.type_function = (v: any): boolean => typeof v === datatypes.type.function

fn.type_func = fn.type_function // backward compatibility

fn.type_object = (v: any): boolean => typeof v === datatypes.type.object

fn.type_of = (v: any, n: string) : boolean => typeof v === n

fn.type_number = (v: any): boolean => typeof v === datatypes.type.number

fn.type_same = (v: any, o: any): boolean => typeof v === typeof o

fn.type_string = (v: any): boolean => typeof v === datatypes.type.string

// type.symbol = (v: any): boolean => typeof v === datatypes.type.symbol

fn.type_undefined = (v: any): boolean => typeof v === datatypes.type.undefined



/******************************************************************************
    Instance (instanceof) Methods
******************************************************************************/
fn.instance_$flags = { }

fn.instance_array = (v: any): boolean => v instanceof Array

fn.instance_boolean = (v: any): boolean => v instanceof Boolean

fn.instance_date = (v: any): boolean => v instanceof Date

fn.instance_error = (v: any): boolean => v instanceof Error

fn.instance_function = (v: any): boolean => v instanceof Function

fn.instance_func = fn.instance_function // backward compatibility

fn.instance_number = (v: any): boolean => v instanceof Number

fn.instance_object = (v: any): boolean => v instanceof Object

fn.instance_of = (v: any, f: () => void): boolean => v instanceof f

fn.instance_string = (v: any): boolean => v instanceof String



/******************************************************************************
    String Type (typeof) Methods
******************************************************************************/
fn.stype_$flags = { }

fn.stype_array = (v: any): boolean => Object.prototype.toString.call(v) === datatypes.stype.array

fn.stype_boolean = (v: any): boolean => Object.prototype.toString.call(v) === datatypes.stype.boolean

fn.stype_date = (v: any): boolean => Object.prototype.toString.call(v) === datatypes.stype.date

fn.stype_error = (v: any): boolean => Object.prototype.toString.call(v) === datatypes.stype.error

fn.stype_function = (v: any): boolean => Object.prototype.toString.call(v) === datatypes.stype.function

fn.stype_func = fn.stype_function // backward compatibility

fn.stype_null = (v: any): boolean => Object.prototype.toString.call(v) === datatypes.stype.null

fn.stype_number = (v: any): boolean => Object.prototype.toString.call(v) === datatypes.stype.number

fn.stype_object = (v: any): boolean => Object.prototype.toString.call(v) === datatypes.stype.object

fn.stype_of = (v: any, n: string): boolean => Object.prototype.toString.call(v) === n

fn.stype_regexp = (v: any): boolean => Object.prototype.toString.call(v) === datatypes.stype.regexp

fn.stype_same = (v: any, o: any): boolean => Object.prototype.toString.call(v) === Object.prototype.toString.call(o)

fn.stype_string = (v: any): boolean => Object.prototype.toString.call(v) === datatypes.stype.string

// stype.symbol = (v: any): boolean => Object.prototype.toString.call(v) === datatypes.stype.symbol

fn.stype_undefined = (v: any): boolean => Object.prototype.toString.call(v) === datatypes.stype.undefined



/******************************************************************************
    Configurable (Based on Configuration) Methods
******************************************************************************/
fn.conf_$flags = { cf: true }

fn.conf_missing = (v: any, c: any): boolean => { return !fn.base_exists(v) || (c.asMissing.indexOf(v) >= 0) }



/******************************************************************************
    Base (Non Short Circuiting) Methods
******************************************************************************/
fn.base_$flags = { }
// Is [v] not undefined and not null?
fn.base_exists = (v: any): boolean => typeof v !== 'undefined' && v !== null
// Does [v] evaluate to false?
fn.base_falsey = (v: any): boolean => v ? false : true
// Is [v] null?
fn.base_null = (v: any): boolean => v === null
// Is [v] equal to itself?
fn.base_self = (v: any): boolean => v === v
// Does [v] evaluate to true?
fn.base_truthy = (v: any): boolean => v ? true : false
// Is [v] undefined?
fn.base_undefined = (v: any): boolean => v === undefined



/******************************************************************************
    Core (Short Circuiting) Methods
******************************************************************************/
fn.core_$flags = { sc: true }

fn.core_after = (v: any, d: any): boolean => new Date(v).getTime() > new Date(d).getTime()

fn.core_array = (v: any): boolean => fn.stype_array(v)

fn.core_before = (v: any, d: any): boolean => new Date(v).getTime() < new Date(d).getTime()

fn.core_between = (v: any, a: any, b: any): boolean => (a < b && v >= a && v <= b) || (a >= b && v <= a && v >= b)

fn.core_boolean = (v: any): boolean => fn.stype_boolean(v)

fn.core_contains = (v: any, c: any): boolean => Utils.indexOf(v, c, false) >= 0

fn.core_cube = (v: any): boolean => (<any>Math).cbrt(v) % 1 === 0

fn.core_date = (v: any): boolean => !isNaN(new Date(v).getTime())

fn.core_discrete = (v: any): boolean => v + 1 !== v

fn.core_divisibleBy = (v: any, d: number): boolean => v % d === 0

fn.core_email = (v: any): boolean => /^[^ ]+?@[^ @]+(\.[^ @]+)+$/i.test(v)

fn.core_empty = (v: any): boolean => v.length === 0

fn.core_endsWith = (v: any, c: any): boolean => Utils.indexOf(v, c, true) === 0

fn.core_equal = (v: any, c: any): boolean => v === c

fn.core_even = (v: any): boolean => Number(String(v).slice(-1)) % 2 === 0

fn.core_false = (v: any): boolean => v === false

fn.core_finite = (v: any): boolean => isFinite(v)

fn.core_float = (v: any): boolean => v % 1 !== 0

fn.core_function = (v: any): boolean => fn.stype_function(v)

fn.core_func = fn.core_function // backward compatibility

fn.core_infinity = (v: any): boolean => v === Infinity

fn.core_integer = (v: any): boolean => v % 1 === 0

fn.core_ipAddress = (v: any): boolean => Const.re.ipAddress.test(v)

fn.core_ipOctet = (v: any): boolean => Const.re.ipOctet.test(v)

fn.core_itemIn = (v: any, a: any): boolean => a.indexOf(v) >= 0

fn.core_json = (v: any): boolean => !fn.stype_undefined(JSON.parse(v))

fn.core_length = (v: any, c: number): boolean => v.length === c

fn.core_less = (v: any, c: any): boolean => v < c

fn.core_like = (v: any, o: any): boolean => v == o

fn.core_longer = (v: any, c: number): boolean => v.length > c

fn.core_match = (v: any, r: any, f: string): boolean => new RegExp(r,f).test(v)

fn.core_more = (v: any, c: any): boolean => v > c

fn.core_nan = (v: any): boolean => isNaN(v)

fn.core_negative = (v: any): boolean => v < 0

fn.core_number = (v: any): boolean => fn.stype_number(v) && !isNaN(v)

fn.core_object = (v: any): boolean => fn.stype_object(v)

fn.core_odd = (v: any): boolean => Number(String(v).slice(-1)) % 2 === 1

fn.core_pattern = (v: any): boolean => new RegExp(v) ? true : false

fn.core_positive = (v: any): boolean => v > 0

fn.core_quad = (v: any): boolean => Math.sqrt(Math.sqrt(v)) % 1 === 0

fn.core_regexp = (v: any): boolean => fn.stype_regexp(v)

fn.core_reversible = (v: any): boolean => String(v) === String(v).split('').reverse().join('')

fn.core_shorter = (v: any, c: number): boolean => v.length < c

fn.core_square = (v: any): boolean => Math.sqrt(v) % 1 === 0

fn.core_startsWith = (v: any, c: any): boolean => Utils.indexOf(v, c, false) === 0

fn.core_string = (v: any): boolean => fn.stype_string(v)

fn.core_thenable = (v: any): boolean => fn.stype_function(v.then)

fn.core_trimable = (v: any): boolean => /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/.test(v)

fn.core_true = (v: any): boolean => v === true

fn.core_url = (v: any): boolean => /^([a-z]+:\/\/)?[^ @]+?(\.[^ @]+?)+?(:\d+)?(\/[^ ]*)?$/i.test(v)

fn.core_zero = (v: any): boolean => v === 0


/*
    String character composition/inclusion test methods
 */

fn.core_alpha = (v: any, c: number): boolean => Utils.matchRepeats(v, '[A-Za-z]', c)

fn.core_alphanumeric = (v: any, c: number): boolean => Utils.matchRepeats(v, '[0-9A-Za-z]', c)

fn.core_binary = (v: any, c: number): boolean => Utils.matchRepeats(v, '[01]', c)

fn.core_decimal = (v: any, c: number): boolean => Utils.matchRepeats(v, '[0-9]', c)

fn.core_hexadecimal = (v: any, c: number): boolean => Utils.matchRepeats(v, '[0-9A-Fa-f]', c)

fn.core_lowercase = (v: any, c: number): boolean => Utils.matchRepeats(v, '[a-z]', c)

fn.core_nonary = (v: any, c: number): boolean => Utils.matchRepeats(v, '[0-8]', c)

fn.core_numeric = (v: any, c: number): boolean => Utils.matchRepeats(v, '[0-9]', c)

fn.core_octal = (v: any, c: number): boolean => Utils.matchRepeats(v, '[0-7]', c)

fn.core_printable = (v: any, c: number): boolean => Utils.matchRepeats(v, '[ -~]', c)

fn.core_punctuation = (v: any, c: number): boolean => Utils.matchRepeats(v, '[!-\\/:-@\\[-`\\{-~]', c)

fn.core_quarternary = (v: any, c: number): boolean => Utils.matchRepeats(v, '[0-3]', c)

fn.core_quinary = (v: any, c: number): boolean => Utils.matchRepeats(v, '[0-4]', c)

fn.core_senary = (v: any, c: number): boolean => Utils.matchRepeats(v, '[0-5]', c)

fn.core_septenary = (v: any, c: number): boolean => Utils.matchRepeats(v, '[0-6]', c)

fn.core_ternary = (v: any, c: number): boolean => Utils.matchRepeats(v, '[0-2]', c)

fn.core_unary = (v: any, c: number): boolean => Utils.matchRepeats(v, '0', c)

fn.core_uppercase = (v: any, c: number): boolean => Utils.matchRepeats(v, '[A-Z]', c)

fn.core_word = (v: any, c: number): boolean => Utils.matchRepeats(v, '\\w', c)

fn.core_whitespace = (v: any, c: number): boolean => Utils.matchRepeats(v, '\\s', c)


/*
    Object information methods.
 */

fn.core_configurable = (v: any, p: string): boolean => Utils.descriptor(v, p).configurable ? true : false

fn.core_enumerable = (v: any, p: string): boolean => Utils.descriptor(v, p).enumerable ? true : false

fn.core_extensible = (v: any): boolean => Object.isExtensible(v)

fn.core_frozen = (v: any): boolean => Object.isFrozen(v)

fn.core_hasKey = (v: any, k: any): boolean => k in v

fn.core_hasOwn = (v: any, k: any): boolean => v.hasOwnProperty(k)

fn.core_sealed = (v: any): boolean => Object.isSealed(v)

fn.core_writable = (v: any, p: string): boolean => Utils.descriptor(v, p).writable ? true : false


/*
    Date and time identification methods.
 */

// Does [v] date fall on the dawn hour?
fn.core_dawn = (v: any): boolean => new Date(v).getUTCHours() === 6
// Does [v] date fall on the dusk hour?
fn.core_dusk = (v: any): boolean => new Date(v).getUTCHours() === 18
// Does [v] date fall on [p] hour of the day?
fn.core_hour = (v: any, p: number): boolean => new Date(v).getUTCHours() === p
// Does [v] date fall on the midnight hour?
fn.core_midnight = (v: any): boolean => new Date(v).getUTCHours() === 0
// Does [v] date fall on [p] minute of the hour?
fn.core_minute = (v: any, p: number): boolean => new Date(v).getUTCMinutes() === p
// Does [v] date fall on [p] month of the year?
fn.core_month = (v: any, p: number): boolean => new Date(v).getUTCMonth() === p
// Does [v] date fall on [p] day of the month?
fn.core_monthDay = (v: any, p: number): boolean => new Date(v).getUTCDate() === p
// Does [v] date fall on the noon hour?
fn.core_noon = (v: any): boolean => new Date(v).getUTCHours() === 12
// Does [v] date fall on [p] second of the minute?
fn.core_second = (v: any, p: number): boolean => new Date(v).getUTCSeconds() === p
// Does [v] date fall on [p] day of the week?
fn.core_weekDay = (v: any, p: number): boolean => new Date(v).getUTCDay() === p
// Does [v] date fall in the year of [p]?
fn.core_year = (v: any, p: number): boolean => new Date(v).getUTCFullYear() === p


/*
    Days of the week.
 */

fn.core_sunday = (v: any): boolean => new Date(v).getUTCDay() === 0

fn.core_monday = (v: any): boolean => new Date(v).getUTCDay() === 1

fn.core_tuesday = (v: any): boolean => new Date(v).getUTCDay() === 2

fn.core_wednesday = (v: any): boolean => new Date(v).getUTCDay() === 3

fn.core_thursday = (v: any): boolean => new Date(v).getUTCDay() === 4

fn.core_friday = (v: any): boolean => new Date(v).getUTCDay() === 5

fn.core_saturday = (v: any): boolean => new Date(v).getUTCDay() === 6


/*
    Months of the year.
 */

fn.core_january = (v: any): boolean => new Date(v).getUTCMonth() === 0

fn.core_february = (v: any): boolean => new Date(v).getUTCMonth() === 1

fn.core_march = (v: any): boolean => new Date(v).getUTCMonth() === 2

fn.core_april = (v: any): boolean => new Date(v).getUTCMonth() === 3

fn.core_may = (v: any): boolean => new Date(v).getUTCMonth() === 4

fn.core_june = (v: any): boolean => new Date(v).getUTCMonth() === 5

fn.core_july = (v: any): boolean => new Date(v).getUTCMonth() === 6

fn.core_august = (v: any): boolean => new Date(v).getUTCMonth() === 7

fn.core_september = (v: any): boolean => new Date(v).getUTCMonth() === 8

fn.core_october = (v: any): boolean => new Date(v).getUTCMonth() === 9

fn.core_november = (v: any): boolean => new Date(v).getUTCMonth() === 10

fn.core_december = (v: any): boolean => new Date(v).getUTCMonth() === 11



/******************************************************************************
    Extension Includer Methods
******************************************************************************/
fn.in_$flags = { sc: true }



/******************************************************************************
    Extension Matcher Methods
******************************************************************************/
fn.matches_$flags = { sc: true }



/******************************************************************************
    Extension Custom Methods
******************************************************************************/
fn.m_$flags = { sc: true }
