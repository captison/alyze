
export let type =
{
    boolean: typeof false,
    function: typeof Function,
    object: typeof {},
    number: typeof 0,
    string: typeof '',
    // symbol: typeof Symbol(''),
    undefined: typeof undefined
}

export let stype =
{
    array: Object.prototype.toString.call(new Array()),
    boolean: Object.prototype.toString.call(new Boolean()),
    date: Object.prototype.toString.call(new Date()),
    error: Object.prototype.toString.call(new Error()),
    function: Object.prototype.toString.call(new Function()),
    null: Object.prototype.toString.call(null),
    number: Object.prototype.toString.call(new Number()),
    object: Object.prototype.toString.call(new Object()),
    regexp: Object.prototype.toString.call(new RegExp('.')),
    string: Object.prototype.toString.call(new String()),
    // symbol: Object.prototype.toString.call(Symbol()),
    undefined: Object.prototype.toString.call(undefined)
}
