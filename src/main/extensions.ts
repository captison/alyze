import * as Utils from './utils';
import { fn } from './functions';
import * as classes from './classes';


export function extend(extensions: Object, prefix?: string): void
{
    Utils.loop(extensions, function(value: any, name: string)
    {
        let extName = prefix ? prefix + Utils.capitalize(name) : name;
        // try to add extension method for all extendables
        Utils.loop(Extenders, (e,k) => e(extName, value));
        // recursively lood for more stuff to extend
        if (fn.stype_object(value)) extend(value, extName);
    });
}


module Extenders
{
    export function matcher(name: string, value: any): void
    {
        if (fn.stype_regexp(value))
        {
            let method = function(v: any): boolean { return value.test(v); }
            classes.prototypeMethod(name, method, 'matches', { sc: true });
        }
    }

    export function custom(name: string, value: any): void
    {
        if (fn.stype_function(value))
        {
            let method = function(v: any, ...a: any[]): boolean { return value.apply(null, [v].concat(a)); }
            classes.prototypeMethod(name, method, 'm', { sc: true });
        }
    }

    export function includer(name: string, value: any): void
    {
        if (fn.type_function(value.indexOf) && fn.type_number(value.length) && value.length > 0)
        {
            if (fn.type_object(value[0]))
            {
                Utils.loop(value[0], (v,k) => { includer(name + Utils.capitalize(k), Utils.toSet(value, k)); });
            }
            else
            {
                let method = function(v: any): boolean { return value.indexOf(v) >= 0; }
                classes.prototypeMethod(name, method, 'in', { sc: true });
            }
        }
    }
}
