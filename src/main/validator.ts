import * as Utils from './utils';
import * as classes from './classes';


/**
    Configuration interface.
 */
export interface Config
{
    onMissingValue?: boolean;
    asMissing?: any;
    negate?: boolean;
}

export { extend } from './extensions';


class Core
{
    _c: any;

    static listGroups = ['any', 'all'];
    static mergeGroups = ['conf', 'base', 'core'];
    static classes: any = Core.prepClasses(Core, classes.generate());

    static prepClasses(target: any, classes: any)
    {
        Utils.loop(classes, function(item, key)
        {
            if (Core.listGroups.indexOf(key) >= 0)
                Core.prepClasses(item, item);
            else if (Core.mergeGroups.indexOf(key) >= 0)
                Utils.merge(target.prototype, item.prototype);
        });
        // remove merged items
        Utils.loop(Core.mergeGroups, function(g) { if (classes[g]) delete classes[g]; });

        return classes;
    }

    constructor(config: Config)
    {
        config = this._c =
        {
            onMissing: Utils.ensure(config.onMissingValue, false),
            asMissing: Utils.ensure(config.asMissing, []),
            negate: config.negate
        }

        let instantiate = function(target: any, classes: any): void
        {
            Utils.loop(classes, function(item, key)
            {
                target[key] = new item(config);
                if (Core.listGroups.indexOf(key) >= 0)
                    instantiate(target[key], item);
            });
        }

        instantiate(this, Core.classes);
    }
}


/*
    The Alyze validator class
 */
export class Validator extends Core
{
    not: Core;

    constructor(config: Config)
    {
        super(Utils.merge({}, config, { negate: false }));
        // create negated method configuration on [this.not] member
        this.not = new Core(Utils.merge({}, config, { negate: true }));
    }
}
