import { log } from '../log';
import * as Utils from './utils';
import { fn } from './functions';


function create(key?: string): any
{
    var klass = function(config: any): void
    {
        this._c = config;
    }

    klass.prototype._x = key;
    common(klass.prototype);

    return klass;
}


function common(proto: any): any
{
    // missing: returns true if value is 'missing'
    proto._m = function(v: any): boolean { return fn.conf_missing(v, this._c) }
    // exists: returns true if value is not null nor undefined
    proto._e = function(v: any): boolean { return fn.base_exists(v) }
    // option: returns [onMissingValue] if value does not exist
    proto._o = function(v: any, t: () => boolean): boolean { return this._m(v) ? this._c.onMissing : this._n(t); }
    // negate: inverses the result of the function argument if configured
    proto._n = function(t: () => boolean): boolean { return this._c.negate ? !this._t(t) : this._t(t); }
    // try: returns a method that executes [t] in a try block
    proto._t = function(t: () => boolean): boolean { try { return t(); } catch(e) { log.test(t, e); return false; } }
    // key: prepend subType key to [n]
    proto._k = function(n: string): string { return this._e(this._x) ? this._x + '.' + n : n; }
    // update: add method name to its prototype
    proto._u = function(n: string, t: () => boolean): () => boolean { t.prototype._name_ = this._k(n); return t; }

    return proto;
}


function prototypeAll(sc: boolean, func: any): (...a: any[]) => boolean
{
    return function(v: any, ...a: any[]): boolean
    {
        for (let i of [].concat(v))
        {
            // missing values cannot pass SC method tests so return false
            if (sc && this._m(i)) return false;
            // return false when test fails
            if (!func.apply(this, [i].concat(a))) return false;
        }
        // return true since all values passed the test
        return true;
    }
}


function prototypeAny(sc: boolean, func: any): (...a: any[]) => boolean
{
    return function(v: any, ...a: any[]): boolean
    {
        for (let i of [].concat(v))
        {
            // missing values do not affect outcome for SC method tests
            if (sc && this._m(i)) continue;
            // return true when test passes
            if (func.apply(this, [i].concat(a))) return true;
        }
        // return false since no values passed the test
        return false;
    }
}


let c: any = { all: create('all'), any: create('any') };

export function prototypeMethod(name: string, item: (...a: any[]) => boolean, group: string, flags: any): void
{
    // create class for the group if one does not exist already
    let protoGrp = (c[group] = c[group] || create(group)).prototype;
    // create any/all classes for the group if not already existing
    let protoAll = (c.all[group] = c.all[group] || create('all.' + group)).prototype;
    let protoAny = (c.any[group] = c.any[group] || create('any.' + group)).prototype;
    // determine the proper method wrap
    let wrap = flags.cf ? wrapConfigurable : flags.sc ? wrapShortCircuit : wrapNegate;

    if (name === '$flags') return;
    // wrap method and add to group prototype
    protoGrp[name] = wrap(name, item);
    // wrap any/all methods and apply to respective prototypes
    protoAll[name] = wrap(name, prototypeAll(flags.sc, item));
    protoAny[name] = wrap(name, prototypeAny(flags.sc, item));
}


export function generate()
{
    Utils.loop(fn, function(item, key)
    {
        let idx = key.indexOf('_');
        // method group name
        let group = key.slice(0, idx);
        // method name
        let name = key.slice(idx + 1);
        // method is in short-circuited group?
        let flags = fn[group + '_$flags']

        prototypeMethod(name, item, group, flags);
    });

    return c;
}


function wrapConfigurable(name: string, func: () => boolean): (v: any) => boolean
{
    return function(v: any): boolean { return this._n(this._u(name, () => func.apply(this, [v].concat(this._c)))); }
}


/**
    Returns a wrapper function that can negate the results of 'func'.
 */
function wrapNegate(name: string, func: () => boolean): () => boolean
{
    return function(...a: any[]): boolean { return this._n(this._u(name, () => func.apply(this, a))); }
}


/**
    Returns a wrapper function that can short circuit 'func'.
 */
function wrapShortCircuit(name: string, func: (...a: any[]) => boolean): () => boolean
{
    return function(...a: any[]): boolean { return this._o(a[0], this._u(name, () => func.apply(this, a))); }
}
