/*
    Alyze Constants

 */

// general constants
export let gen: any = { }
// from http://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex
gen.escapeChars = [ '-', '[', ']', '/', '{', '}', '(', ')', '*', '+', '?', '.', '\\', '^', '$', '|' ]

// string regular expressions
export let sre: any = { }
// range 0 to 255
sre.ipOctet = "(\\d|([1-9]|1\\d|2[0-4])\\d|25[0-5])"
// range 0 to 65535
// c.sre._0_65535 = "(\\d)"
// ip address (version 4)
sre.ipAddress = "(" + sre.ipOctet + "\\.){3}" + sre.ipOctet

// regular expression objects
export let re: any = { }

re.escapeChars = new RegExp('[' + gen.escapeChars.join('\\') + ']', 'g')
// ip address (version 4)
re.ipAddress = new RegExp("^" + sre.ipAddress + "$")
// ip octet (version 4)
re.ipOctet = new RegExp("^" + sre.ipOctet + "$")
