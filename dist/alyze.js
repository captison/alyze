/*! alyze v0.0.6 @Tue, 28 Feb 2017 22:49:59 GMT */
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["alyze"] = factory();
	else
		root["alyze"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.l = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };

/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};

/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};

/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 8);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var Const = __webpack_require__(3);
var Types = __webpack_require__(5);
/*
    Converts the first character of `value` to upper case.

    @param {string} value
        The value to convert to upper case.
    @return {string}
        The capitalized string.
 */
function capitalize(value) {
    return value.slice(0, 1).toUpperCase() + value.slice(1);
}
exports.capitalize = capitalize;
function descriptor(object, property) {
    return Object.getOwnPropertyDescriptor(object, property) || {};
}
exports.descriptor = descriptor;
function ensure(value, def) {
    return typeof value === 'undefined' || value === null ? def : value;
}
exports.ensure = ensure;
function escapeRE(value) {
    return value.replace(Const.re.escapeChars, "\\$&");
}
exports.escapeRE = escapeRE;
function find(item, path) {
    if (path === void 0) { path = ''; }
    if (path.length > 0 && typeof item === 'object') {
        var index = path.indexOf('.');
        return index >= 0 ? find(item[path.slice(0, index)], path.slice(index + 1)) : find(item[path]);
    }
    else if (path.length === 0) {
        return item;
    }
}
exports.find = find;
/**
    Loops through the owned properties of `object` calling `func` with the
    value and key for each entry.

    @param {object} object
        The object to be iterated.
    @param {function} func
        The callback for each iteration.
    @return {array}
        Keys of `object`.
 */
function loop(object, func) {
    var array = [];
    for (var key in object || {}) {
        if (object.hasOwnProperty(key)) {
            array.push(key);
            if (func)
                func(object[key], key);
        }
    }
    return array;
}
exports.loop = loop;
/**
    Checks `value` against the regular expression in the following way:
        - if count is undefined, is entire `value` a repetitive match
          against `re`? (i.e. /^(re)+$/)
        - if count is more than 0, does `value` have at least `count`
          matches for 're'? (i.e. /re/g)
        - if count is less than 0, does `value` have at most `count`
          matches for 're'? (i.e. /re/g)
        - otherwise, does `value` have zero matches for 're'? (i.e. /re/g)

    @param {*} value
        Test value.
    @param {string} re
        Regular expression to test against.
    @param {number} [count]
        Minimal number of times the regular expression must match.
    @return {boolean}
        True if the match check passes.
 */
function matchRepeats(value, re, count) {
    if (typeof count === 'undefined' || count === null)
        return new RegExp('^(' + re + ')+$').test(value);
    else if (count < 0)
        return matchCount(value, new RegExp(re, 'g')) >= Math.abs(count);
    else
        return matchCount(value, new RegExp(re, 'g')) === count;
}
exports.matchRepeats = matchRepeats;
/**
    Returns the number of times `regex` matches `value`.

    @return {number}
        The match count.
 */
function matchCount(value, regex) {
    return (String(value).match(regex) || []).length;
}
exports.matchCount = matchCount;
/**
    Copies all properties from `objects` into `target` object in the order
    given. If a value for a property is undefined then it is skipped.

    @param {object} target
        The merging target. A new object is created if not specified.
    @param {...object} objects
        The object(s) to be merged. Null items are skipped.
    @return {array}
        The target object.
 */
function merge(target) {
    var objects = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        objects[_i - 1] = arguments[_i];
    }
    target = target || {};
    for (var _a = 0, objects_1 = objects; _a < objects_1.length; _a++) {
        var object = objects_1[_a];
        loop(object, function (v, k) { if (typeof v !== 'undefined')
            target[k] = v; });
    }
    return target;
}
exports.merge = merge;
/**
    Checks `value` for inclusion of `search`.

    If `value` is a string, then 'search' is coerced to string and an
    .indexOf() test is performed.

    If `value` is an array, then `search` is coerced into an array and
    the sequence of values in `search` is looked for in `value`.

    If `value` is not an array or string then an array containing only `value`
    is checked against `search`.

    @param {any} value
        The value to be checked.
    @param {any} search
        The value to check for.
    @param {boolean} reverse
        Reverse `value` and `search` before comparison (check from end)?
    @return
        The index of `search` in `value`.
 */
function indexOf(value, search, reverse) {
    if (Object.prototype.toString.call(value) === Types.stype.string) {
        if (reverse)
            return value.split('').reverse().join('').indexOf(String(search).split('').reverse().join(''));
        else
            return value.indexOf(search);
    }
    else if (Array.isArray(value)) {
        var vItem = reverse ? [].concat(value).reverse() : value;
        var sItem = reverse ? [].concat(search).reverse() : [].concat(search);
        // as all strings contain the empty string, so must all arrays...
        if (sItem.length === 0)
            return 0;
        var idx = vItem.indexOf(sItem[0]);
        while (idx >= 0) {
            var matched = true, segment = vItem.slice(idx, idx + sItem.length);
            // check value segment for match against search
            for (var i = 0, imx = sItem.length; i < imx; i++) {
                if (!(matched = matched && segment[i] === sItem[i]))
                    break;
            }
            if (matched)
                break;
            else
                idx = vItem.indexOf(sItem[0], idx + 1);
        }
        return idx;
    }
    else {
        return [value].indexOf(search);
    }
}
exports.indexOf = indexOf;
/**
    Returns a set of values derived from `path` in the `objects` array.

    @param {array} objects
        The array of objects having the values to obtain.
    @param {string} path
        The path in `objects` to obtain values from.
    @return {array}
        Set of unique values from the `path` in `objects`.
 */
function toSet(objects, path) {
    var array = [];
    for (var _i = 0, objects_2 = objects; _i < objects_2.length; _i++) {
        var object = objects_2[_i];
        var value = find(object, path);
        if (typeof value !== 'undefined' && array.indexOf(value) < 0)
            array.push(value);
    }
    return array;
}
exports.toSet = toSet;


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var Log = (function () {
    function Log() {
        this.testMsg = '[%e] was caught for test method %m - "false" was returned.';
        this.out = function () { };
    }
    // info(...args: any[]): void { this.log('[ INFO ]', args); }
    Log.prototype.warn = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        this.log('[ WARN ]', args);
    };
    // fail(...args: any[]): void { this.log('[FAILED]', args); }
    Log.prototype.test = function (test, error) {
        this.warn(this.testMsg.replace('%m', test.prototype._name_).replace('%e', error + ''));
    };
    Log.prototype.log = function (level, args) { this.out.apply(null, [level, 'alyze:'].concat(args)); };
    Log.prototype.set = function (out) { if (typeof out === 'function')
        this.out = out; };
    return Log;
}());
exports.log = new Log();


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var log_1 = __webpack_require__(1);
var Utils = __webpack_require__(0);
var functions_1 = __webpack_require__(4);
function create(key) {
    var klass = function (config) {
        this._c = config;
    };
    klass.prototype._x = key;
    common(klass.prototype);
    return klass;
}
function common(proto) {
    // missing: returns true if value is 'missing'
    proto._m = function (v) { return functions_1.fn.conf_missing(v, this._c); };
    // exists: returns true if value is not null nor undefined
    proto._e = function (v) { return functions_1.fn.base_exists(v); };
    // option: returns [onMissingValue] if value does not exist
    proto._o = function (v, t) { return this._m(v) ? this._c.onMissing : this._n(t); };
    // negate: inverses the result of the function argument if configured
    proto._n = function (t) { return this._c.negate ? !this._t(t) : this._t(t); };
    // try: returns a method that executes [t] in a try block
    proto._t = function (t) { try {
        return t();
    }
    catch (e) {
        log_1.log.test(t, e);
        return false;
    } };
    // key: prepend subType key to [n]
    proto._k = function (n) { return this._e(this._x) ? this._x + '.' + n : n; };
    // update: add method name to its prototype
    proto._u = function (n, t) { t.prototype._name_ = this._k(n); return t; };
    return proto;
}
function prototypeAll(sc, func) {
    return function (v) {
        var a = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            a[_i - 1] = arguments[_i];
        }
        for (var _a = 0, _b = [].concat(v); _a < _b.length; _a++) {
            var i = _b[_a];
            // missing values cannot pass SC method tests so return false
            if (sc && this._m(i))
                return false;
            // return false when test fails
            if (!func.apply(this, [i].concat(a)))
                return false;
        }
        // return true since all values passed the test
        return true;
    };
}
function prototypeAny(sc, func) {
    return function (v) {
        var a = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            a[_i - 1] = arguments[_i];
        }
        for (var _a = 0, _b = [].concat(v); _a < _b.length; _a++) {
            var i = _b[_a];
            // missing values do not affect outcome for SC method tests
            if (sc && this._m(i))
                continue;
            // return true when test passes
            if (func.apply(this, [i].concat(a)))
                return true;
        }
        // return false since no values passed the test
        return false;
    };
}
var c = { all: create('all'), any: create('any') };
function prototypeMethod(name, item, group, flags) {
    // create class for the group if one does not exist already
    var protoGrp = (c[group] = c[group] || create(group)).prototype;
    // create any/all classes for the group if not already existing
    var protoAll = (c.all[group] = c.all[group] || create('all.' + group)).prototype;
    var protoAny = (c.any[group] = c.any[group] || create('any.' + group)).prototype;
    // determine the proper method wrap
    var wrap = flags.cf ? wrapConfigurable : flags.sc ? wrapShortCircuit : wrapNegate;
    if (name === '$flags')
        return;
    // wrap method and add to group prototype
    protoGrp[name] = wrap(name, item);
    // wrap any/all methods and apply to respective prototypes
    protoAll[name] = wrap(name, prototypeAll(flags.sc, item));
    protoAny[name] = wrap(name, prototypeAny(flags.sc, item));
}
exports.prototypeMethod = prototypeMethod;
function generate() {
    Utils.loop(functions_1.fn, function (item, key) {
        var idx = key.indexOf('_');
        // method group name
        var group = key.slice(0, idx);
        // method name
        var name = key.slice(idx + 1);
        // method is in short-circuited group?
        var flags = functions_1.fn[group + '_$flags'];
        prototypeMethod(name, item, group, flags);
    });
    return c;
}
exports.generate = generate;
function wrapConfigurable(name, func) {
    return function (v) {
        var _this = this;
        return this._n(this._u(name, function () { return func.apply(_this, [v].concat(_this._c)); }));
    };
}
/**
    Returns a wrapper function that can negate the results of 'func'.
 */
function wrapNegate(name, func) {
    return function () {
        var _this = this;
        var a = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            a[_i] = arguments[_i];
        }
        return this._n(this._u(name, function () { return func.apply(_this, a); }));
    };
}
/**
    Returns a wrapper function that can short circuit 'func'.
 */
function wrapShortCircuit(name, func) {
    return function () {
        var _this = this;
        var a = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            a[_i] = arguments[_i];
        }
        return this._o(a[0], this._u(name, function () { return func.apply(_this, a); }));
    };
}


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/*
    Alyze Constants

 */

// general constants
exports.gen = {};
// from http://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex
exports.gen.escapeChars = ['-', '[', ']', '/', '{', '}', '(', ')', '*', '+', '?', '.', '\\', '^', '$', '|'];
// string regular expressions
exports.sre = {};
// range 0 to 255
exports.sre.ipOctet = "(\\d|([1-9]|1\\d|2[0-4])\\d|25[0-5])";
// range 0 to 65535
// c.sre._0_65535 = "(\\d)"
// ip address (version 4)
exports.sre.ipAddress = "(" + exports.sre.ipOctet + "\\.){3}" + exports.sre.ipOctet;
// regular expression objects
exports.re = {};
exports.re.escapeChars = new RegExp('[' + exports.gen.escapeChars.join('\\') + ']', 'g');
// ip address (version 4)
exports.re.ipAddress = new RegExp("^" + exports.sre.ipAddress + "$");
// ip octet (version 4)
exports.re.ipOctet = new RegExp("^" + exports.sre.ipOctet + "$");


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var Utils = __webpack_require__(0);
var Const = __webpack_require__(3);
var datatypes = __webpack_require__(5);
exports.fn = {};
/******************************************************************************
    Type (typeof) Methods
******************************************************************************/
exports.fn.type_$flags = {};
exports.fn.type_boolean = function (v) { return typeof v === datatypes.type.boolean; };
exports.fn.type_function = function (v) { return typeof v === datatypes.type.function; };
exports.fn.type_func = exports.fn.type_function; // backward compatibility
exports.fn.type_object = function (v) { return typeof v === datatypes.type.object; };
exports.fn.type_of = function (v, n) { return typeof v === n; };
exports.fn.type_number = function (v) { return typeof v === datatypes.type.number; };
exports.fn.type_same = function (v, o) { return typeof v === typeof o; };
exports.fn.type_string = function (v) { return typeof v === datatypes.type.string; };
// type.symbol = (v: any): boolean => typeof v === datatypes.type.symbol
exports.fn.type_undefined = function (v) { return typeof v === datatypes.type.undefined; };
/******************************************************************************
    Instance (instanceof) Methods
******************************************************************************/
exports.fn.instance_$flags = {};
exports.fn.instance_array = function (v) { return v instanceof Array; };
exports.fn.instance_boolean = function (v) { return v instanceof Boolean; };
exports.fn.instance_date = function (v) { return v instanceof Date; };
exports.fn.instance_error = function (v) { return v instanceof Error; };
exports.fn.instance_function = function (v) { return v instanceof Function; };
exports.fn.instance_func = exports.fn.instance_function; // backward compatibility
exports.fn.instance_number = function (v) { return v instanceof Number; };
exports.fn.instance_object = function (v) { return v instanceof Object; };
exports.fn.instance_of = function (v, f) { return v instanceof f; };
exports.fn.instance_string = function (v) { return v instanceof String; };
/******************************************************************************
    String Type (typeof) Methods
******************************************************************************/
exports.fn.stype_$flags = {};
exports.fn.stype_array = function (v) { return Object.prototype.toString.call(v) === datatypes.stype.array; };
exports.fn.stype_boolean = function (v) { return Object.prototype.toString.call(v) === datatypes.stype.boolean; };
exports.fn.stype_date = function (v) { return Object.prototype.toString.call(v) === datatypes.stype.date; };
exports.fn.stype_error = function (v) { return Object.prototype.toString.call(v) === datatypes.stype.error; };
exports.fn.stype_function = function (v) { return Object.prototype.toString.call(v) === datatypes.stype.function; };
exports.fn.stype_func = exports.fn.stype_function; // backward compatibility
exports.fn.stype_null = function (v) { return Object.prototype.toString.call(v) === datatypes.stype.null; };
exports.fn.stype_number = function (v) { return Object.prototype.toString.call(v) === datatypes.stype.number; };
exports.fn.stype_object = function (v) { return Object.prototype.toString.call(v) === datatypes.stype.object; };
exports.fn.stype_of = function (v, n) { return Object.prototype.toString.call(v) === n; };
exports.fn.stype_regexp = function (v) { return Object.prototype.toString.call(v) === datatypes.stype.regexp; };
exports.fn.stype_same = function (v, o) { return Object.prototype.toString.call(v) === Object.prototype.toString.call(o); };
exports.fn.stype_string = function (v) { return Object.prototype.toString.call(v) === datatypes.stype.string; };
// stype.symbol = (v: any): boolean => Object.prototype.toString.call(v) === datatypes.stype.symbol
exports.fn.stype_undefined = function (v) { return Object.prototype.toString.call(v) === datatypes.stype.undefined; };
/******************************************************************************
    Configurable (Based on Configuration) Methods
******************************************************************************/
exports.fn.conf_$flags = { cf: true };
exports.fn.conf_missing = function (v, c) { return !exports.fn.base_exists(v) || (c.asMissing.indexOf(v) >= 0); };
/******************************************************************************
    Base (Non Short Circuiting) Methods
******************************************************************************/
exports.fn.base_$flags = {};
// Is [v] not undefined and not null?
exports.fn.base_exists = function (v) { return typeof v !== 'undefined' && v !== null; };
// Does [v] evaluate to false?
exports.fn.base_falsey = function (v) { return v ? false : true; };
// Is [v] null?
exports.fn.base_null = function (v) { return v === null; };
// Is [v] equal to itself?
exports.fn.base_self = function (v) { return v === v; };
// Does [v] evaluate to true?
exports.fn.base_truthy = function (v) { return v ? true : false; };
// Is [v] undefined?
exports.fn.base_undefined = function (v) { return v === undefined; };
/******************************************************************************
    Core (Short Circuiting) Methods
******************************************************************************/
exports.fn.core_$flags = { sc: true };
exports.fn.core_after = function (v, d) { return new Date(v).getTime() > new Date(d).getTime(); };
exports.fn.core_array = function (v) { return exports.fn.stype_array(v); };
exports.fn.core_before = function (v, d) { return new Date(v).getTime() < new Date(d).getTime(); };
exports.fn.core_between = function (v, a, b) { return (a < b && v >= a && v <= b) || (a >= b && v <= a && v >= b); };
exports.fn.core_boolean = function (v) { return exports.fn.stype_boolean(v); };
exports.fn.core_contains = function (v, c) { return Utils.indexOf(v, c, false) >= 0; };
exports.fn.core_cube = function (v) { return Math.cbrt(v) % 1 === 0; };
exports.fn.core_date = function (v) { return !isNaN(new Date(v).getTime()); };
exports.fn.core_discrete = function (v) { return v + 1 !== v; };
exports.fn.core_divisibleBy = function (v, d) { return v % d === 0; };
exports.fn.core_email = function (v) { return /^[^ ]+?@[^ @]+(\.[^ @]+)+$/i.test(v); };
exports.fn.core_empty = function (v) { return v.length === 0; };
exports.fn.core_endsWith = function (v, c) { return Utils.indexOf(v, c, true) === 0; };
exports.fn.core_equal = function (v, c) { return v === c; };
exports.fn.core_even = function (v) { return Number(String(v).slice(-1)) % 2 === 0; };
exports.fn.core_false = function (v) { return v === false; };
exports.fn.core_finite = function (v) { return isFinite(v); };
exports.fn.core_float = function (v) { return v % 1 !== 0; };
exports.fn.core_function = function (v) { return exports.fn.stype_function(v); };
exports.fn.core_func = exports.fn.core_function; // backward compatibility
exports.fn.core_infinity = function (v) { return v === Infinity; };
exports.fn.core_integer = function (v) { return v % 1 === 0; };
exports.fn.core_ipAddress = function (v) { return Const.re.ipAddress.test(v); };
exports.fn.core_ipOctet = function (v) { return Const.re.ipOctet.test(v); };
exports.fn.core_itemIn = function (v, a) { return a.indexOf(v) >= 0; };
exports.fn.core_json = function (v) { return !exports.fn.stype_undefined(JSON.parse(v)); };
exports.fn.core_length = function (v, c) { return v.length === c; };
exports.fn.core_less = function (v, c) { return v < c; };
exports.fn.core_like = function (v, o) { return v == o; };
exports.fn.core_longer = function (v, c) { return v.length > c; };
exports.fn.core_match = function (v, r, f) { return new RegExp(r, f).test(v); };
exports.fn.core_more = function (v, c) { return v > c; };
exports.fn.core_nan = function (v) { return isNaN(v); };
exports.fn.core_negative = function (v) { return v < 0; };
exports.fn.core_number = function (v) { return exports.fn.stype_number(v) && !isNaN(v); };
exports.fn.core_object = function (v) { return exports.fn.stype_object(v); };
exports.fn.core_odd = function (v) { return Number(String(v).slice(-1)) % 2 === 1; };
exports.fn.core_pattern = function (v) { return new RegExp(v) ? true : false; };
exports.fn.core_positive = function (v) { return v > 0; };
exports.fn.core_quad = function (v) { return Math.sqrt(Math.sqrt(v)) % 1 === 0; };
exports.fn.core_regexp = function (v) { return exports.fn.stype_regexp(v); };
exports.fn.core_reversible = function (v) { return v === String(v).split('').reverse().join(''); };
exports.fn.core_shorter = function (v, c) { return v.length < c; };
exports.fn.core_square = function (v) { return Math.sqrt(v) % 1 === 0; };
exports.fn.core_startsWith = function (v, c) { return Utils.indexOf(v, c, false) === 0; };
exports.fn.core_string = function (v) { return exports.fn.stype_string(v); };
exports.fn.core_thenable = function (v) { return exports.fn.stype_function(v.then); };
exports.fn.core_trimable = function (v) { return /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/.test(v); };
exports.fn.core_true = function (v) { return v === true; };
exports.fn.core_url = function (v) { return /^([a-z]+:\/\/)?[^ @]+?(\.[^ @]+?)+?(:\d+)?(\/[^ ]*)?$/i.test(v); };
exports.fn.core_zero = function (v) { return v === 0; };
/*
    String character composition/inclusion test methods
 */
exports.fn.core_alpha = function (v, c) { return Utils.matchRepeats(v, '[A-Za-z]', c); };
exports.fn.core_alphanumeric = function (v, c) { return Utils.matchRepeats(v, '[0-9A-Za-z]', c); };
exports.fn.core_binary = function (v, c) { return Utils.matchRepeats(v, '[01]', c); };
exports.fn.core_decimal = function (v, c) { return Utils.matchRepeats(v, '[0-9]', c); };
exports.fn.core_hexadecimal = function (v, c) { return Utils.matchRepeats(v, '[0-9A-Fa-f]', c); };
exports.fn.core_lowercase = function (v, c) { return Utils.matchRepeats(v, '[a-z]', c); };
exports.fn.core_nonary = function (v, c) { return Utils.matchRepeats(v, '[0-8]', c); };
exports.fn.core_numeric = function (v, c) { return Utils.matchRepeats(v, '[0-9]', c); };
exports.fn.core_octal = function (v, c) { return Utils.matchRepeats(v, '[0-7]', c); };
exports.fn.core_printable = function (v, c) { return Utils.matchRepeats(v, '[ -~]', c); };
exports.fn.core_punctuation = function (v, c) { return Utils.matchRepeats(v, '[!-\\/:-@\\[-`\\{-~]', c); };
exports.fn.core_quarternary = function (v, c) { return Utils.matchRepeats(v, '[0-3]', c); };
exports.fn.core_quinary = function (v, c) { return Utils.matchRepeats(v, '[0-4]', c); };
exports.fn.core_senary = function (v, c) { return Utils.matchRepeats(v, '[0-5]', c); };
exports.fn.core_septenary = function (v, c) { return Utils.matchRepeats(v, '[0-6]', c); };
exports.fn.core_ternary = function (v, c) { return Utils.matchRepeats(v, '[0-2]', c); };
exports.fn.core_unary = function (v, c) { return Utils.matchRepeats(v, '0', c); };
exports.fn.core_uppercase = function (v, c) { return Utils.matchRepeats(v, '[A-Z]', c); };
exports.fn.core_word = function (v, c) { return Utils.matchRepeats(v, '\\w', c); };
exports.fn.core_whitespace = function (v, c) { return Utils.matchRepeats(v, '\\s', c); };
/*
    Object information methods.
 */
exports.fn.core_configurable = function (v, p) { return Utils.descriptor(v, p).configurable ? true : false; };
exports.fn.core_enumerable = function (v, p) { return Utils.descriptor(v, p).enumerable ? true : false; };
exports.fn.core_extensible = function (v) { return Object.isExtensible(v); };
exports.fn.core_frozen = function (v) { return Object.isFrozen(v); };
exports.fn.core_hasKey = function (v, k) { return k in v; };
exports.fn.core_hasOwn = function (v, k) { return v.hasOwnProperty(k); };
exports.fn.core_sealed = function (v) { return Object.isSealed(v); };
exports.fn.core_writable = function (v, p) { return Utils.descriptor(v, p).writable ? true : false; };
/*
    Date and time identification methods.
 */
// Does [v] date fall on the dawn hour?
exports.fn.core_dawn = function (v) { return new Date(v).getUTCHours() === 6; };
// Does [v] date fall on the dusk hour?
exports.fn.core_dusk = function (v) { return new Date(v).getUTCHours() === 18; };
// Does [v] date fall on [p] hour of the day?
exports.fn.core_hour = function (v, p) { return new Date(v).getUTCHours() === p; };
// Does [v] date fall on the midnight hour?
exports.fn.core_midnight = function (v) { return new Date(v).getUTCHours() === 0; };
// Does [v] date fall on [p] minute of the hour?
exports.fn.core_minute = function (v, p) { return new Date(v).getUTCMinutes() === p; };
// Does [v] date fall on [p] month of the year?
exports.fn.core_month = function (v, p) { return new Date(v).getUTCMonth() === p; };
// Does [v] date fall on [p] day of the month?
exports.fn.core_monthDay = function (v, p) { return new Date(v).getUTCDate() === p; };
// Does [v] date fall on the noon hour?
exports.fn.core_noon = function (v) { return new Date(v).getUTCHours() === 12; };
// Does [v] date fall on [p] second of the minute?
exports.fn.core_second = function (v, p) { return new Date(v).getUTCSeconds() === p; };
// Does [v] date fall on [p] day of the week?
exports.fn.core_weekDay = function (v, p) { return new Date(v).getUTCDay() === p; };
// Does [v] date fall in the year of [p]?
exports.fn.core_year = function (v, p) { return new Date(v).getUTCFullYear() === p; };
/*
    Days of the week.
 */
exports.fn.core_sunday = function (v) { return new Date(v).getUTCDay() === 0; };
exports.fn.core_monday = function (v) { return new Date(v).getUTCDay() === 1; };
exports.fn.core_tuesday = function (v) { return new Date(v).getUTCDay() === 2; };
exports.fn.core_wednesday = function (v) { return new Date(v).getUTCDay() === 3; };
exports.fn.core_thursday = function (v) { return new Date(v).getUTCDay() === 4; };
exports.fn.core_friday = function (v) { return new Date(v).getUTCDay() === 5; };
exports.fn.core_saturday = function (v) { return new Date(v).getUTCDay() === 6; };
/*
    Months of the year.
 */
exports.fn.core_january = function (v) { return new Date(v).getUTCMonth() === 0; };
exports.fn.core_february = function (v) { return new Date(v).getUTCMonth() === 1; };
exports.fn.core_march = function (v) { return new Date(v).getUTCMonth() === 2; };
exports.fn.core_april = function (v) { return new Date(v).getUTCMonth() === 3; };
exports.fn.core_may = function (v) { return new Date(v).getUTCMonth() === 4; };
exports.fn.core_june = function (v) { return new Date(v).getUTCMonth() === 5; };
exports.fn.core_july = function (v) { return new Date(v).getUTCMonth() === 6; };
exports.fn.core_august = function (v) { return new Date(v).getUTCMonth() === 7; };
exports.fn.core_september = function (v) { return new Date(v).getUTCMonth() === 8; };
exports.fn.core_october = function (v) { return new Date(v).getUTCMonth() === 9; };
exports.fn.core_november = function (v) { return new Date(v).getUTCMonth() === 10; };
exports.fn.core_december = function (v) { return new Date(v).getUTCMonth() === 11; };
/******************************************************************************
    Extension Includer Methods
******************************************************************************/
exports.fn.in_$flags = { sc: true };
/******************************************************************************
    Extension Matcher Methods
******************************************************************************/
exports.fn.matches_$flags = { sc: true };
/******************************************************************************
    Extension Custom Methods
******************************************************************************/
exports.fn.m_$flags = { sc: true };


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.type = {
    boolean: typeof false,
    function: typeof Function,
    object: typeof {},
    number: typeof 0,
    string: typeof '',
    // symbol: typeof Symbol(''),
    undefined: typeof undefined
};
exports.stype = {
    array: Object.prototype.toString.call(new Array()),
    boolean: Object.prototype.toString.call(new Boolean()),
    date: Object.prototype.toString.call(new Date()),
    error: Object.prototype.toString.call(new Error()),
    function: Object.prototype.toString.call(new Function()),
    null: Object.prototype.toString.call(null),
    number: Object.prototype.toString.call(new Number()),
    object: Object.prototype.toString.call(new Object()),
    regexp: Object.prototype.toString.call(new RegExp('.')),
    string: Object.prototype.toString.call(new String()),
    // symbol: Object.prototype.toString.call(Symbol()),
    undefined: Object.prototype.toString.call(undefined)
};


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Utils = __webpack_require__(0);
var classes = __webpack_require__(2);
var extensions_1 = __webpack_require__(7);
exports.extend = extensions_1.extend;
var Core = (function () {
    function Core(config) {
        config = this._c =
            {
                onMissing: Utils.ensure(config.onMissingValue, false),
                asMissing: Utils.ensure(config.asMissing, []),
                negate: config.negate
            };
        var instantiate = function (target, classes) {
            Utils.loop(classes, function (item, key) {
                target[key] = new item(config);
                if (Core.listGroups.indexOf(key) >= 0)
                    instantiate(target[key], item);
            });
        };
        instantiate(this, Core.classes);
    }
    Core.prepClasses = function (target, classes) {
        Utils.loop(classes, function (item, key) {
            if (Core.listGroups.indexOf(key) >= 0)
                Core.prepClasses(item, item);
            else if (Core.mergeGroups.indexOf(key) >= 0)
                Utils.merge(target.prototype, item.prototype);
        });
        // remove merged items
        Utils.loop(Core.mergeGroups, function (g) { if (classes[g])
            delete classes[g]; });
        return classes;
    };
    return Core;
}());
Core.listGroups = ['any', 'all'];
Core.mergeGroups = ['conf', 'base', 'core'];
Core.classes = Core.prepClasses(Core, classes.generate());
/*
    The Alyze validator class
 */
var Validator = (function (_super) {
    __extends(Validator, _super);
    function Validator(config) {
        var _this = _super.call(this, Utils.merge({}, config, { negate: false })) || this;
        // create negated method configuration on [this.not] member
        _this.not = new Core(Utils.merge({}, config, { negate: true }));
        return _this;
    }
    return Validator;
}(Core));
exports.Validator = Validator;


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var Utils = __webpack_require__(0);
var functions_1 = __webpack_require__(4);
var classes = __webpack_require__(2);
function extend(extensions, prefix) {
    Utils.loop(extensions, function (value, name) {
        var extName = prefix ? prefix + Utils.capitalize(name) : name;
        // try to add extension method for all extendables
        Utils.loop(Extenders, function (e, k) { return e(extName, value); });
        // recursively lood for more stuff to extend
        if (functions_1.fn.stype_object(value))
            extend(value, extName);
    });
}
exports.extend = extend;
var Extenders;
(function (Extenders) {
    function matcher(name, value) {
        if (functions_1.fn.stype_regexp(value)) {
            var method = function (v) { return value.test(v); };
            classes.prototypeMethod(name, method, 'matches', { sc: true });
        }
    }
    Extenders.matcher = matcher;
    function custom(name, value) {
        if (functions_1.fn.stype_function(value)) {
            var method = function (v) {
                var a = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    a[_i - 1] = arguments[_i];
                }
                return value.apply(null, [v].concat(a));
            };
            classes.prototypeMethod(name, method, 'm', { sc: true });
        }
    }
    Extenders.custom = custom;
    function includer(name, value) {
        if (functions_1.fn.type_function(value.indexOf) && functions_1.fn.type_number(value.length) && value.length > 0) {
            if (functions_1.fn.type_object(value[0])) {
                Utils.loop(value[0], function (v, k) { includer(name + Utils.capitalize(k), Utils.toSet(value, k)); });
            }
            else {
                var method = function (v) { return value.indexOf(v) >= 0; };
                classes.prototypeMethod(name, method, 'in', { sc: true });
            }
        }
    }
    Extenders.includer = includer;
})(Extenders || (Extenders = {}));


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var log_1 = __webpack_require__(1);
var Main = __webpack_require__(6);
var Utils = __webpack_require__(0);
// default configuration
var defaultConfig = {};
// global configuration
function config(config) {
    if (config === void 0) { config = {}; }
    log_1.log.set(config.log);
    // add any specified extensions
    if (config.extend)
        Main.extend(config.extend);
    // set default onMissingValue
    if (typeof config.onMissingValue === 'boolean')
        defaultConfig.onMissingValue = config.onMissingValue;
    // set default asMissing
    if (config.asMissing)
        defaultConfig.asMissing = config.asMissing;
    return this;
}
exports.config = config;
// create new validator instance and return
function create(config) {
    if (config === void 0) { config = {}; }
    return new Main.Validator(Utils.merge({}, defaultConfig, config));
}
exports.create = create;


/***/ })
/******/ ]);
});