module.exports =
[
    { "name": "january", "abbr": "jan" },
    { "name": "february", "abbr": "feb" },
    { "name": "march", "abbr": "mar" },
    { "name": "april", "abbr": "apr" },
    { "name": "may", "abbr": "may" },
    { "name": "june", "abbr": "jun" },
    { "name": "july", "abbr": "jul" },
    { "name": "august", "abbr": "aug" },
    { "name": "september", "abbr": "sep" },
    { "name": "october", "abbr": "oct" },
    { "name": "november", "abbr": "nov" },
    { "name": "december", "abbr": "dec" }
]
