/**
    Luhn algorithm credit card number validation.

    Adapted from orginal javascript from here:
    https://gist.github.com/DiegoSalazar/4075533
 */
module.exports = function(v)
{
    v = String(v);  // coerce to string
    if (!/^\d+$/.test(v)) return false;

  	var nCheck = 0, nDigit = 0, bEven = false;

  	for (var n = v.length - 1; n >= 0; n--)
    {
  		  var cDigit = v.charAt(n);
  			var nDigit = parseInt(cDigit, 10);

        if (bEven && (nDigit *= 2) > 9) nDigit -= 9;

        nCheck += nDigit;
        bEven = !bEven;
    }

  	return (nCheck % 10) == 0;
}
