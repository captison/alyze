module.exports =
{
    value: '',
    params:
    {
        'type.named': [ [ 'object', 'string' ] ],
        'type.of': 'string',
        'type.same': 'null',
        'instance.named': [ [ 'string', 'object' ] ],
        'instance.of': Object,
        'stype.named': 'error',
        'stype.of': '[object String]',
        'stype.same': null,

        'after': new Date('2017-10-14'),
        'before': new Date('2017-10-14'),
        'contains': 'empty',
        'divisibleBy': 0,
        'endsWith': '',
        'hasKey': 'indexOf',
        'hasOwn': 'length',
        'hour': 10,
        'itemIn': [ [ '2016-08-12' ] ],
        'length': 10,
        'less': 'abc',
        'match': /^\d{4}-\d{2}-\d{2}$/,
        'more': 'abc',
        'longer': 12,
        'startsWith': '',
        'shorter': 12,
        'weekDay': 5,
        'year': 2016,
    },
    expect: { main: true, string: false },
    ignore: [ 'hasKey', 'json', 'func' ],
    tests:
    {
        main:
        [
            'exists', 'falsey', 'self',

            'stype.of', 'stype.string',

            'type.of', 'type.named', 'type.same', 'type.string',

            'cube', 'discrete', 'empty', 'endsWith', 'even', 'finite', 'frozen', 'hasOwn',
            'integer', 'less', 'pattern', 'quad', 'reversible', 'sealed', 'shorter', 'startsWith',
            'square', 'string',
        ],
        string:
        [
            'null', 'truthy', 'undefined',

            'instance.array', 'instance.boolean', 'instance.date', 'instance.error',
            'instance.function', 'instance.object', 'instance.of', 'instance.named',
            'instance.number', 'instance.string',

            'stype.array', 'stype.boolean', 'stype.date', 'stype.error',
            'stype.function', 'stype.null', 'stype.named', 'stype.number', 'stype.object',
            'stype.regexp', 'stype.same', 'stype.undefined',

            'type.boolean', 'type.function', 'instance.named',  'type.number', 'type.object',
            'type.undefined',
        ]
    }
}
