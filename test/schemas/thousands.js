module.exports =
{
    value: 4096,
    params:
    {
        'type.named': 'object, number',
        'type.of': 'number',
        'type.same': 1024,
        'instance.named': 'object, number',
        'instance.of': Number,
        'stype.named': 'object, number',
        'stype.of': Object.prototype.toString.call(16),
        'stype.same': 2048,

        'after': new Date('2017-10-14'),
        'before': new Date('2017-10-14'),
        'between': [ 2400, 7200 ],
        'contains': '40',
        'divisibleBy': 128,
        'endsWith': 4096,
        'equal': String('   '),
        'hasKey': 'length',
        'hasOwn': 'length',
        'hour': 0,
        'itemIn': [ [ '  ', ' ', 18 ] ],
        'length': 10,
        'less': 6400,
        'like': new Number(4096),
        'longer': 12,
        'match': /^ +$/,
        'more': 3200,
        'shorter': 12,
        'startsWith': 'four',
        'weekDay': 5,
        'year': 4096,
    },
    expect: { main: true },
    ignore: [ 'hasKey' ],
    tests:
    {
        main:
        [
            'exists', 'self', 'truthy',

            'stype.named', 'stype.number', 'stype.of', 'stype.same',

            'type.named', 'type.of', 'type.number', 'type.same',

            'alphanumeric', 'before', 'between', 'cube', 'decimal', 'discrete',
            'date', 'divisibleBy', 'endsWith', 'even', 'finite', 'frozen', 'hexadecimal',
            'hour', 'integer', 'january', 'json', 'less', 'like', 'midnight', 'more', 'number',
            'numeric', 'pattern', 'positive', 'printable', 'quad', 'sealed', 'square',
            'thursday', 'word'
        ]
    }
}
