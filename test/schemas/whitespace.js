module.exports =
{
    value: new String('   '),
    params:
    {
        'type.named': [ [ 'string', 'object' ] ],
        'type.of': 'object',
        'type.same': new String('undefined'),
        'instance.named': [ [ 'string', 'object' ] ],
        'instance.of': String,
        'stype.named': [ [ 'string', 'object' ] ],
        'stype.of': Object.prototype.toString.call('undefined'),
        'stype.same': 'null',

        'after': new Date('2017-10-14'),
        'before': new Date('2017-10-14'),
        'contains': '  ',
        'divisibleBy': 0,
        'endsWith': ' ',
        'equal': String('   '),
        'hasKey': 'indexOf',
        'hasOwn': 'length',
        'hour': 0,
        'itemIn': [ [ '  ', ' ', 18 ] ],
        'length': 10,
        'less': 'abc',
        'like': 0,
        'longer': 12,
        'match': /^ +$/,
        'more': 'abc',
        'shorter': 12,
        'startsWith': ' ',
        'weekDay': 5,
        'year': 0,
    },
    expect: { main: true },
    ignore: [ 'hasKey', 'json' ],
    tests:
    {
        main:
        [
            'exists', 'self', 'truthy',

            'instance.named', 'instance.of', 'instance.object', 'instance.string',

            'stype.named', 'stype.of', 'stype.same', 'stype.string',

            'type.named', 'type.of', 'type.object', 'type.same',

            'contains', 'cube', 'discrete', 'endsWith', 'even', 'extensible', 'finite',
            'hasKey', 'hasOwn', 'integer', 'less', 'like', 'match', 'quad', 'pattern',
            'printable', 'square', 'shorter', 'startsWith', 'string', 'trimable', 'whitespace',
        ]
    }
}
