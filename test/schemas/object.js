module.exports =
{
    value: { name: 'Jim', age: 46, married: new Date('1997-02-12') },
    params:
    {
        'type.named': [ [ 'object', 'string' ] ],
        'type.of': 'string',
        'type.same': undefined,
        'instance.named': [ [ 'string', 'object' ] ],
        'instance.of': Object,
        'stype.named': 'object',
        'stype.of': '[object Object]',
        'stype.same': new Object(),

        'after': new Date('2017-10-14'),
        'before': new Date('2017-10-14'),
        'contains': '{}',
        'configurable': 'constructor',
        'divisibleBy': 0,
        'endsWith': '}',
        'enumerable': 'married',
        'hasKey': 'age',
        'hasOwn': 'name',
        'hour': 15,
        'itemIn': [ [ '2016-08-12' ] ],
        'length': 10,
        'less': 'abc',
        'match': /^\d{4}-\d{2}-\d{2}$/,
        'more': 'abc',
        'longer': 50,
        'shorter': Number.MAX_VALUE,
        'weekDay': 5,
        'writable': 'phone',
        'startsWith': '{',
        'year': 2016,
    },
    expect: { main: true },
    ignore: [ 'json' ],
    tests:
    {
        main:
        [
            'exists', 'self', 'truthy',

            'instance.named', 'instance.object', 'instance.of',

            'stype.named', 'stype.object', 'stype.of', 'stype.same',

            'type.named', 'type.object',

            'discrete', 'enumerable', 'extensible', 'float', 'hasKey', 'hasOwn',
            'less', 'nan', 'object', 'pattern', 'printable'
        ]
    }
}
