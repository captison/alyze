module.exports =
{
    value: {},
    params:
    {
        'type.named': [ [ 'undefined', 'string', 'number' ] ],
        'type.of': 'object',
        'type.same': {},
        'instance.named': [ [ 'string', 'object' ] ],
        'instance.of': Object,
        'stype.named': 'object',
        'stype.of': '[object Number]',
        'stype.same': new Object(),

        'after': new Date('2017-10-14'),
        'before': new Date('2017-10-14'),
        'configurable': 'constructor',
        'contains': {},
        'divisibleBy': 0,
        'endsWith': 'ect]',
        'enumerable': 'constructor',
        'hasKey': 'constructor',
        'hasOwn': 'constructor',
        'hour': 15,
        'itemIn': [ [ '2016-08-12' ] ],
        'length': 10,
        'less': 'abc',
        'match': /^\[.*\]$/,
        'more': '[]',
        'longer': 1,
        'startsWith': '[obj',
        'shorter': Number.MAX_VALUE,
        'weekDay': 3,
        'writable': 'constructor',
        'year': 2016,
    },
    expect: { main: true },
    ignore: [ 'json' ],
    tests:
    {
        main:
        [
            'exists', 'self', 'truthy',

            'instance.named', 'instance.object', 'instance.of',

            'stype.named', 'stype.object', 'stype.same',

            'type.of', 'type.object', 'type.same',

            'discrete', 'extensible', 'float', 'hasKey',
            'less', 'match', 'more', 'nan', 'object', 'pattern', 'printable'
        ]
    }
}
