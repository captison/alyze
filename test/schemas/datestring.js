module.exports =
{
    value: '2016-08-12',
    params:
    {
        'type.named': 'func, number',
        'type.of': String,
        'type.same': undefined,
        'instance.named': 'string',
        'instance.of': String,
        'stype.named': 'func, object',
        'stype.of': Object.prototype.toString.call('undefined'),
        'stype.same': undefined,

        'after': new Date('2017-10-14'),
        'before': new Date('2017-10-14'),
        'contains': '08',
        'divisibleBy': 0,
        'endsWith': '12',
        'hasKey': 'indexOf',
        'hasOwn': 'length',
        'hexadecimal': -11,
        'hour': 10,
        'itemIn': [ [ '2016-08-12' ] ],
        'length': 10,
        'less': 'abc',
        'longer': 12,
        'lowercase': 0,
        'match': /^\d{4}-\d{2}-\d{2}$/,
        'more': 'abc',
        'shorter': 12,
        'startsWith': 2016,
        'uppercase': 0,
        'weekDay': 5,
        'year': 2016,
    },
    expect: { main: true },
    ignore: [ 'hasKey', 'json' ],
    tests:
    {
        main:
        [
            'exists', 'self', 'truthy',

            'stype.of', 'stype.string',

            'type.string',

            'august', 'before', 'contains', 'date', 'discrete', 'even', 'endsWith', 'float', 'friday',
            'frozen', 'hasOwn', 'itemIn', 'length', 'less', 'lowercase', 'match', 'midnight',
            'nan', 'pattern', 'printable', 'sealed', 'shorter', 'startsWith',
            'string', 'uppercase', 'weekDay', 'year',
        ]
    }
}
