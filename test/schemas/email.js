module.exports =
{
    value: 'maple.bacon@donut.com',
    params:
    {
        'type.named': [ [ 'number', 'string', 'object' ] ],
        'type.of': 'string',
        'type.same': 'string',
        'instance.named': [ [ 'string', 'object' ] ],
        'instance.of': String,
        'stype.named': [ [ 'string', 'object' ] ],
        'stype.of': Object.prototype.toString.call(17),
        'stype.same': 'NaN',

        'after': new Date('2017-10-14'),
        'before': new Date('2017-10-14'),
        'contains': 'bacon',
        'divisibleBy': 0,
        'endsWith': 'pork',
        'hasKey': 'indexOf',
        'hasOwn': 'length',
        'hour': 10,
        'itemIn': [ [ 'spaz' ] ],
        'length': 10,
        'less': 'abc',
        'match': /^([a-z]+:\/\/)?[^ ]+?(\.[^ ]+?)+?(:\d+)?(\/[^ ]*)?$/i,
        'more': 'abc',
        'longer': 12,
        'lowercase': 18,
        'shorter': 12,
        'startsWith': 'pork',
        'weekDay': 0,
        'year': 2025,
    },
    expect: { main: true },
    ignore: [ 'hasKey', 'json' ],
    tests:
    {
        main:
        [
            'exists', 'self', 'truthy',

            'stype.named', 'stype.same', 'stype.string',

            'type.named', 'type.of', 'type.same', 'type.string',

            'contains', 'discrete', 'email', 'float', 'frozen',
            'hasOwn', 'longer', 'lowercase', 'match', 'more', 'nan', 'pattern',
            'printable', 'sealed', 'string',
        ]
    }
}
