module.exports =
{
    value: true,
    params:
    {
        'type.named': 'object, number',
        'type.of': 'number',
        'type.same': false,
        'instance.named': 'object, number',
        'instance.of': Number,
        'stype.named': 'boolean, number',
        'stype.of': '[object Boolean]',
        'stype.same': new Boolean('false'),

        'contains': 'false',
        'divisibleBy': 1,
        'endsWith': 'u',
        'equal': ''.length === 0,
        'hasKey': 'length',
        'hasOwn': 'length',
        'hour': 0,
        'itemIn': [ [ NaN, true, false, Infinity, null ] ],
        'length': 4,
        'less': 6400,
        'like': new Number(0),
        'longer': 12,
        'match': /true/,
        'more': -3200,
        'shorter': 12,
        'startsWith': true,
        'weekDay': 0,
        'year': 0,
    },
    expect: { main: true },
    ignore: [ 'hasKey' ],
    tests:
    {
        main:
        [
            'exists', 'self', 'truthy',

            'stype.boolean', 'stype.named', 'stype.of', 'stype.same',

            'type.boolean', 'type.same',

            'alpha', 'alphanumeric', 'boolean', 'discrete', 'cube', 'date', 'divisibleBy',
            'equal', 'finite', 'frozen', 'hour', 'integer', 'itemIn', 'january', 'json',
            'less', 'lowercase', 'match', 'midnight', 'more', 'positive', 'pattern',
            'printable', 'quad', 'sealed', 'square', 'startsWith', 'thursday', 'true', 'word'
        ]
    }
}
