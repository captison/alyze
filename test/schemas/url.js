module.exports =
{
    value: 'https://www.npmjs.com/package/straints',
    params:
    {
        'type.named': [ [ 'number', 'string', 'object' ] ],
        'type.of': 'string',
        'type.same': 'string',
        'instance.named': [ [ 'string', 'object' ] ],
        'instance.of': RegExp,
        'stype.named': [ [ 'string', 'object' ] ],
        'stype.of': Object.prototype.toString.call(17),
        'stype.same': 'NaN',

        'after': new Date('2017-10-14'),
        'before': new Date('2017-10-14'),
        'contains': 'npmjs',
        'divisibleBy': 0,
        'endsWith': 'any',
        'hasKey': 'indexOf',
        'hasOwn': 'length',
        'hour': 0,
        'itemIn': [ [ 'spaz' ] ],
        'length': 47,
        'less': 'abc',
        'longer': 12,
        'lowercase': 0,
        'match': /^.+$/,
        'more': 'ftp://',
        'shorter': 300,
        'startsWith': 'any',
        'uppercase': 0,
        'weekDay': 0,
        'year': 2025,
    },
    expect: { main: true },
    ignore: [ 'hasKey', 'json' ],
    tests:
    {
        main:
        [
            'exists', 'self', 'truthy',

            'stype.named', 'stype.same', 'stype.string',

            'type.named', 'type.of', 'type.same', 'type.string',

            'contains', 'discrete', 'float', 'frozen', 'hasOwn', 'longer',
            'match', 'more', 'nan', 'pattern', 'printable', 'sealed', 'shorter',
            'string', 'uppercase', 'url'
        ]
    }
}
