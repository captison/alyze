module.exports =
{
    value: undefined,
    params:
    {
        'type.of': Object,
        'type.named': [ 'undefined' ],
        'type.same': [ undefined ],
        'instance.of': Object,
        'instance.named': [ 'number', 'string', 'regexp' ],
        'stype.of': Object.prototype.toString.call(undefined),
        'stype.named': [ 'object' ],
        'stype.same': [ undefined ],

        'itemIn': [ [ undefined ] ],
    },
    expect: { main: true, option: false, string: false },
    ignore: [ 'contains', 'endsWith', 'func', 'startsWith' ],
    tests:
    {
        main:
        [
            'falsey', 'missing', 'self', 'undefined',

            'stype.of', 'stype.same', 'stype.undefined',

            'type.named', 'type.same', 'type.undefined',
        ],
        option:
        [
            'exists', 'null', 'truthy',

            'instance.array', 'instance.boolean', 'instance.date', 'instance.error',
            'instance.function', 'instance.named', 'instance.number', 'instance.object',
            'instance.of', 'instance.string',

            'stype.array', 'stype.boolean', 'stype.date', 'stype.error',
            'stype.function', 'stype.null', 'stype.named', 'stype.number', 'stype.object',
            'stype.regexp', 'stype.string',

            'type.boolean', 'type.function', 'type.number', 'type.object', 'type.of',
            'type.string',
        ],
        string: 'option'
    }
}
