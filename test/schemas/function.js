module.exports =
{
    value: function which(one, two, pig) { return pig ? one : two; },
    params:
    {
        'type.named': 'function',
        'type.of': 'function',
        'type.same': new Function(),
        'instance.named': [ [ 'string', 'object' ] ],
        'instance.of': Function,
        'stype.named': 'object',
        'stype.of': '[object Function]',
        'stype.same': new Function(),

        'contains': 'pig',
        'configurable': 'constructor',
        'divisibleBy': 0,
        'enumerable': 'name',
        'hasKey': 'name',
        'hasOwn': 'length',
        'itemIn': [ [ new Function() ] ],
        'length': 3,
        'longer': 5,
        'match': "^function",
        'shorter': 4,
        'writable': 'phone',
        'year': 2016,
    },
    expect: { main: true },
    ignore: [ 'json' ],
    tests:
    {
        main:
        [
            'exists', 'self', 'truthy',

            'instance.function', 'instance.func', 'instance.object', 'instance.of',

            'stype.func', 'stype.function', 'stype.of', 'stype.same',

            'type.func', 'type.function', 'type.of', 'type.same',

            'discrete', 'extensible', 'float', 'func', 'function', 'hasKey', 'hasOwn', 'length',
            'match', 'nan', 'pattern', 'printable', 'shorter',
        ]
    }
}
