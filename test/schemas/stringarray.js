module.exports =
{
    value: [ 'refrigerator', 'oven', 'blender' ],
    params:
    {
        'type.of': 'object',
        'type.same': new Array(),
        'instance.of': String,
        'stype.of': Object.prototype.toString.call([ null ]),
        'stype.same': '/array/',

        'after': 900800700,
        'before': 900800700,
        'divisibleBy': 128,
        'endsWith': 'blender',
        'equal': 'refrigerator',
        'hasKey': 'length',
        'hasOwn': 'length',
        'hour': 0,
        'itemIn': [ [ new Error(), 'blender', 8675309 ] ],
        'length': 4,
        'less': 6400,
        'like': [ [ 'refrigerator', 'oven', 'blender' ] ],
        'longer': 3,
        'lowercase': 7,
        'match': /^ +$/,
        'more': 3200,
        'shorter': 4,
        'weekDay': 0,
        'year': 1926,
    },
    expect: { main: true },
    ignore: [ 'hasKey', 'json' ],
    tests:
    {
        main:
        [
            'exists', 'self', 'truthy',

            'all.exists', 'all.self', 'all.truthy',

            'any.exists', 'any.self', 'any.truthy',

            'type.named', 'type.object', 'type.of',

            'all.type.string',

            'any.type.string',

            'instance.array', 'instance.named', 'instance.object',

            'stype.array', 'stype.of',

            'all.stype.same', 'all.stype.string',

            'any.stype.same', 'any.stype.string',

            'array', 'discrete', 'endsWith', 'extensible', 'hasOwn', 'float',
            'nan', 'pattern', 'printable', 'shorter',

            'all.alpha', 'all.alphanumeric', 'all.discrete', 'all.float', 'all.frozen',
            'all.hasOwn', 'all.longer', 'all.nan', 'all.pattern', 'all.printable', 'all.sealed',
            'all.string', 'all.word',

            'any.alpha', 'any.alphanumeric', 'any.discrete', 'any.endsWith',
            'any.equal', 'any.float', 'any.frozen', 'any.hasOwn', 'any.itemIn', 'any.length',
            'any.longer', 'any.lowercase', 'any.nan', 'any.pattern', 'any.printable', 'any.sealed',
            'any.string', 'any.word'
        ]
    }
}
