module.exports =
{
    value: [ /bacon/, 'eggs', new String('cinnamon roll'), 'Hashbrowns', 'ice water', 23 ],
    params:
    {
        'type.named': 'object, number',
        'type.of': 'number',
        'type.same': new Array(),
        'instance.named': 'array, number',
        'instance.of': Array,
        'stype.named': 'object, number',
        'stype.of': Object.prototype.toString.call(null),
        'stype.same': /array/,

        'after': new Date('2017-10-14'),
        'before': new Date('2017-10-14'),
        'contains': [ [ 'Hashbrowns', 'ice water' ] ],
        'divisibleBy': 128,
        'endsWith': [ [ 'ice water', 23 ] ],
        'equal': [ [ /bacon/, 'eggs', new String('cinnamon roll'), 'hashbrowns', 'ice water', 23 ] ],
        'hasKey': 'length',
        'hasOwn': 'length',
        'hour': 0,
        'itemIn': [ [ '  ', ' ', 18 ] ],
        'length': 10,
        'less': 6400,
        'like': [ [ /bacon/, 'eggs', new String('cinnamon roll'), 'hashbrowns', 'ice water', 23 ] ],
        'longer': 3,
        'match': /^ +$/,
        'more': 3200,
        'shorter': 12,
        'startsWith': [ [ /bacon/, 'eggs' ] ],
        'weekDay': 5,
        'year': 4096,
    },
    expect: { main: true },
    ignore: [ 'hasKey', 'json' ],
    tests:
    {
        main:
        [
            'exists', 'self', 'truthy',

            'all.exists', 'all.self', 'all.truthy',

            'any.exists', 'any.self', 'any.truthy',

            'type.named', 'type.object',

            'any.type.number', 'any.type.object', 'any.type.of', 'any.type.string',

            'instance.array', 'instance.named', 'instance.object', 'instance.of',

            'any.instance.object', 'any.instance.string',

            'stype.array',

            'any.stype.number', 'any.stype.regexp', 'any.stype.same', 'any.stype.string',

            'array', 'contains', 'discrete', 'endsWith', 'extensible', 'hasOwn', 'float', 'longer',
            'nan', 'odd', 'pattern', 'printable', 'shorter',

            'all.discrete', 'all.pattern', 'all.printable',

            'any.alpha', 'any.alphanumeric', 'any.before', 'any.date',
            'any.decimal', 'any.discrete', 'any.extensible', 'any.finite',
            'any.float', 'any.frozen', 'any.hasOwn', 'any.hexadecimal', 'any.hour',
            'any.integer', 'any.ipOctet', 'any.january', 'any.length', 'any.less', 'any.longer',
            'any.lowercase', 'any.midnight', 'any.nan', 'any.nonary', 'any.number', 'any.numeric',
            'any.octal', 'any.odd', 'any.pattern', 'any.positive', 'any.printable',
            'any.quarternary', 'any.quinary', 'any.regexp', 'any.sealed', 'any.senary', 'any.septenary',
            'any.shorter', 'any.string', 'any.thursday', 'any.word'
        ]
    }
}
