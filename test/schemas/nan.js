module.exports =
{
    value: NaN,
    params:
    {
        'type.named': 'object, number',
        'type.of': 'number',
        'type.same': 1024,
        'instance.named': 'object, number',
        'instance.of': Number,
        'stype.named': 'object, number',
        'stype.of': Object.prototype.toString.call(Infinity),
        'stype.same': 2048,

        'after': new Date('2017-10-14'),
        'before': new Date('2017-10-14'),
        'contains': 'Nan',
        'divisibleBy': 1,
        'endsWith': 'null',
        'equal': NaN,
        'hasKey': 'constructor',
        'hasOwn': 'constructor',
        'hour': 0,
        'itemIn': [ [ NaN, 25 ] ],
        'length': 10,
        'less': Infinity,
        'like': new Number(NaN),
        'longer': 12,
        'match': /^NaN$/,
        'more': Infinity,
        'shorter': 12,
        'startsWith': NaN,
        'weekDay': 0,
        'year': 0,
    },
    expect: { main: true },
    ignore: [ 'hasKey', 'json' ],
    tests:
    {
        main:
        [
            'exists', 'falsey',

            'stype.named', 'stype.number', 'stype.of', 'stype.same',

            'type.named', 'type.of', 'type.number', 'type.same',

            'alpha', 'alphanumeric', 'discrete', 'float',
            'frozen', 'match', 'nan', 'pattern', 
            'printable', 'sealed', 'word'
        ]
    }
}
