module.exports =
{
    value: [ null, undefined ],
    params:
    {
        'type.of': 'null',
        'type.same': [ [ '' ] ],
        'instance.of': Object,
        'stype.same': new Error(),

        'between': [ 0, Infinity ],
        'contains': 'NaN',
        'divisibleBy': 2,
        'endsWith': 'undefined',
        'itemIn': [ [ null, NaN, true ] ],
        'match': /.+/,
        'startsWith': 'undefined'
    },
    expect: { main: true, option: true },
    ignore: [ 'json' ],
    tests:
    {
        option:
        [
            'exists', 'self', 'truthy',

            'all.falsey', 'all.missing', 'all.self',

            'any.falsey', 'any.missing', 'any.null', 'any.self',
            'any.undefined',

            'type.object', 'type.same',

            'any.type.object', 'any.type.same', 'any.type.undefined',

            'instance.array', 'instance.object', 'instance.of',

            'stype.array',

            'any.stype.null', 'any.stype.undefined',

            'array', 'extensible', 'discrete', 'float', 'match', 'nan',
            'pattern', 'printable', 'punctuation',
        ]
    }
}
