module.exports =
{
    value: null,
    params:
    {
        'type.of': Object,
        'type.named': [ 'object' ],
        'type.same': [ null ],
        'instance.of': Object,
        'instance.named': [ 'string', 'object' ],
        'stype.of': Object.prototype.toString.call(null),
        'stype.named': [ 'null' ],
        'stype.same': [ null ],

        'itemIn': [ [ null ] ],
    },
    expect: { main: true, option: false, string: false },
    ignore: [ 'contains', 'endsWith', 'func', 'startsWith' ],
    tests:
    {
        main:
        [
            'falsey', 'missing', 'null', 'self',

            'stype.of', 'stype.null', 'stype.named', 'stype.same',

            'type.object', 'type.named', 'type.same',
        ],
        option:
        [
            'exists', 'truthy', 'undefined',

            'instance.array', 'instance.boolean', 'instance.date', 'instance.error',
            'instance.function', 'instance.named', 'instance.number', 'instance.object',
            'instance.of', 'instance.string',

            'stype.array', 'stype.boolean', 'stype.date', 'stype.error',
            'stype.function', 'stype.number', 'stype.object', 'stype.regexp',
            'stype.string', 'stype.undefined',

            'type.boolean', 'type.function', 'type.number', 'type.of', 'type.string',
            'type.undefined',
        ],
        string: 'option'
    }
}
