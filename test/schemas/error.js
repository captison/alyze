module.exports =
{
    value: new Error('Symbol'),
    params:
    {
        'type.named': 'object',
        'type.of': 'object',
        'type.same': 'Symbol',
        'instance.named': [ 'object', 'error' ],
        'instance.of': Error,
        'stype.named': 'object',
        'stype.of': Object.prototype.toString.call({}),
        'stype.same': new Error(),

        'after': new Date('2017-10-14'),
        'before': new Date('2017-10-14'),
        'contains': 'Symbol',
        'divisibleBy': 0,
        'endsWith': new Error('Symbol'),
        'equal': String('   '),
        'hasKey': 'stack',
        'hasOwn': 'stack',
        'hour': 0,
        'itemIn': [ [ new Error('Symbol') ] ],
        'length': 10,
        'less': 'Theater',
        'like': new Error('Symbol'),
        'longer': 12,
        'match': "^ +$",
        'more': new Error('Mnemonic'),
        'shorter': 12,
        'startsWith': 'cabbage',
        'weekDay': 5,
        'year': 4096,
    },
    expect: { main: true },
    ignore: [ 'json' ],
    tests:
    {
        main:
        [
            'exists', 'self', 'truthy',

            'instance.error', 'instance.named', 'instance.object', 'instance.of',

            'stype.error', 'stype.same',

            'type.named', 'type.object', 'type.of',

            'discrete', 'extensible', 'float', 'hasKey', 'hasOwn', 'less',
            'more', 'nan', 'pattern', 'printable',
        ]
    }
}
