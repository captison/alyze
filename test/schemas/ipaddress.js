module.exports =
{
    value: '10.1.234.7',
    params:
    {
        'type.named': 'object, number',
        'type.of': 'string',
        'type.same': 1024,
        'instance.named': 'string, number',
        'instance.of': Number,
        'stype.named': 'string, object',
        'stype.of': Object.prototype.toString.call('7'),
        'stype.same': '2048',

        'after': new Date('2017-10-14'),
        'before': new Date('2017-10-14'),
        'between': [ 2400, 7200 ],
        'contains': '40',
        'divisibleBy': 128,
        'endsWith': '7',
        'equal': String('10.1.234.7'),
        'hasOwn': 'length',
        'hour': 0,
        'itemIn': [ [ '?', '10.1.234.7', 1835 ] ],
        'length': 10,
        'less': 6400,
        'like': new String(),
        'longer': 12,
        'match': /\.[0-9]/,
        'more': 3200,
        'shorter': 12,
        'startsWith': '10',
        'weekDay': 5,
        'year': 4096,
    },
    expect: { main: true },
    ignore: [ 'hasKey', 'json' ],
    tests:
    {
        main:
        [
            'exists', 'self', 'truthy',

            'stype.named', 'stype.of', 'stype.same', 'stype.string',

            'type.named', 'type.of', 'type.string',

            'discrete', 'endsWith', 'equal', 'float', 'ipAddress', 'itemIn', 'length',
            'match', 'nan', 'odd', 'pattern', 'shorter', 'startsWith', 'string',
            'printable', 'frozen', 'hasOwn', 'sealed', 'url'
        ]
    }
}
