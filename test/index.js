var test = require('./test');


// test.item('null');
// test.item('undefined');
// test.item('datestring');
// test.item('whitespace');
// test.item('thousands');
// test.item('error');
// test.item('email');
// test.item('url');
// test.item('array');
// test.item('emptystring');
// test.item('object');
// test.item('emptyobject');
// test.item('nan');
// test.item('true');
// test.item('stringarray');
// test.item('badarray');
// test.item('ipaddress');
// test.item('function');

test.all();

// More Coverage Testing
var v = require('../dist/alyze').config({ extend: { only: { spaces: /^ +$/ } } }).create();
v.matches.onlySpaces(' ');
// To test warning
v.json('');
