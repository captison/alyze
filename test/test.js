var assert = require('assert');
var items = require('./schemas');
var extensions = require('./extensions');


var log = console.log;

var beg = new Date();
var alyze = require('../dist/alyze');
var end = new Date();

alyze.config({ extend: extensions, onMissingValue: true, asMissing: [] })
// alyze.config({ log: log });
alyze.config({ log: function() {} });

// log('[ INFO ]', 'app require took', (end.getTime() - beg.getTime()) + 'ms')

var ta = function(one, two, message)
{
    var equal = one === two;
    var status = equal ? '[ PASS ]' : '[FAILED]';

    if(!equal)
        log(status, message);
    assert.equal(one, two, message)
}

var instances =
{
    main: alyze.create({ onMissingValue: false }),
    option: alyze.create(),
    string: alyze.create({ asMissing: [ '' ] })
}

var nonsc = /^(.*?\.)?(exists|falsey|missing|null|self|truthy|undefined|((type|instance|stype)\..*)).*$/;

module.exports.item = function(ik, string, x)
{
    x = x || 0;
    var item = items[ik];
    var ignore = ['constructor'].concat(item.ignore);

    for (var tk in item.tests)
    {
        var tnames = item.tests[tk];
        if (typeof tnames === 'string') tnames = item.tests[tnames];

        var root = instances[tk];

        var doTests = function(v, prefix)
        {
            for (var member in v)
            {
                var name = prefix + (prefix.length > 0 ? '.' : '') + member;

                if (/^_.*/.test(member) || ignore.indexOf(member) >= 0)
                {
                    continue;
                }
                if (typeof v[member] === 'function' && name.indexOf(string || '') >= 0)
                {
                    var notout = name.replace(/^not\./, '');
                    var lstout = notout.replace(/^(any|all)\./, '');
                    var fixout = Array.isArray(item.value) ? notout : lstout;
                    var params = typeof item.params[lstout] !== 'undefined' ? item.params[lstout] : [];
                    var result = v[member].apply(v, [item.value].concat(params));
                    var expect = tnames.indexOf(fixout) >= 0 ? item.expect[tk] : !item.expect[tk];
                    // flip expectation for 'not', if value not missing or non-short curcuit test
                    if ((name.indexOf('not.') === 0) && (!root._m(item.value) || nonsc.test(name)))
                        expect = !expect;

                    var m = [++x, ik + ':' + tk, "{" + item.value + "}", name, "(" + params + ")", expect ].join(' ');
                    ta (result, expect, m);
                }
                else (typeof v[member] === 'object')
                {
                    doTests(v[member], name);
                }
            }
        }

        doTests(root, '');
    }

    return x;
}

module.exports.all = function() { var x = 0; for (var key in items) x = module.exports.item(key, null, x); }
