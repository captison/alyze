
module.exports = function(local_dir, skip_files, local_exports)
{
    var exported = local_exports || {};
    var path = require('path');
    var exts = ['js', 'json'];

    skip_files = skip_files ? [].concat(skip_files) : [];

    var requireAll = function(filename)
    {
        if (skip_files.indexOf(filename) < 0 && filename[0] !== '.')
        {
            var parts = filename.split('.');

            if (exts.indexOf(parts[parts.length - 1]) >= 0)
                filename = filename.split('.').slice(0, -1).join('.');

            exported[filename] = require(path.resolve(local_dir, filename));
        }
    };

    require('fs').readdirSync(local_dir).forEach(requireAll);

    return exported;
}
