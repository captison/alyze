# Alyze

## What is this?

A simple, customizable data validator for javascript.

### Features

- Lots of validation methods
- Control pass or fail on missing values for most methods
- Invert boolean result for all methods (not)
- Configure custom validations for list inclusion and regex matching
- No dependencies

## Sweet! How do I get it?

### npm

```
npm install alyze --save
```

### bower

```
bower install alyze --save
```

## What's the Setup?

### For Node

```js
var alyze = require('alyze');

alyze.config({ ... });
```
To get a validator instance:
```js
var valyzate = alyze.create({ ... });
```
Configuration options (`...`) are optional in both cases above.

### For the Browser

```html
<script src=".../dist/alyze.min.js"></script>
```
Then you can do
```html
<script>
  alyze.config({ ... });

  var valyzate = alyze.create({ ... });

  ...
</script>
```

## How does all this work?

### Configuration Options

All configuration options are aptional.
```js
alyze.config({ ... }) /* OR */ alyze.create({ ... })
```

- `onMissingValue` { boolean }  
For short-circuiting methods, the value that is immediately returned when test value is `null`, `undefined`, or included in `asMissing`.  The default is `false`.

- `asMissing` { array }  
Additional values to be treated as 'missing'. You might want to specify an empty string when working with web form data, for instance.

- `extend` { object }  
Specify custom validation methods (`.config()` only).  See the _Extension Methods_ section for more details.

- `log` { function }  
Errors caught during validation are logged. Specify the log function here (`.config()` only).

### Validation Methods

Some notes about the validation methods.

- All methods are generally designed to perform the simplest check possible (granularity).
- All methods accept a test value as the first argument.
- The test value is coerced to the proper type as necessary.
- All methods have an inverse (`not`) version.
- Many methods short-circuit for missing values by immediately returning `onMissingValue`.

#### Non Short-Circuiting

These methods will evaluate missing test values.

##### Existence/Identity Methods
```js
valyzate.xxx() /* OR */ valyzate.not.xxx()
```
- **exists** ( v ) - Is not `undefined` and not `null`?
- **falsey** ( v ) - Evaluates to `false`?
- **missing** ( v ) - Is `null`, `undefined`, or any `asMissing` value?
- **null** ( v ) - Is equal to null?
- **self** ( v ) - Is equal to itself?
- **truthy** ( v ) - Evaluates to `true`?
- **undefined** ( v ) - Is equal to `undefined`?

##### Type Of Methods

These methods perform `typeof` checks on the test value.
```js
valyzate.type.xxx() /* OR */ valyzate.not.type.xxx()
```
- **boolean** ( v )
- **func** ( v )
- **object** ( v )
- **of** ( v, string ) - Is type of the specified type?
- **number** ( v )
- **same** ( v, any ) - Type matches comparison value's type?
- **string** ( v )
- **undefined** ( v )

##### Instance Of Methods

These methods perform `instanceof` checks on the test value.
```js
valyzate.instance.xxx() /* OR */ valyzate.not.instance.xxx()
```
- **array** ( v )
- **boolean** ( v )
- **date** ( v )
- **error** ( v )
- **func** ( v )
- **number** ( v )
- **object** ( v )
- **of** ( v, class ) - Is instance of `class`?
- **string** ( v )

##### String Type Of Methods

These methods perform `Object.prototype.toString` checks on the test value.
```js
valyzate.stype.xxx() /* OR */ valyzate.not.stype.xxx()
```
- **array** ( v )
- **boolean** ( v )
- **date** ( v )
- **error** ( v )
- **func** ( v )
- **null** ( v )
- **number** ( v )
- **object** ( v )
- **of** ( v, string ) - String type is `string`?
- **regexp** ( v )
- **same** ( v, any ) - String type matches comparison value's string type?
- **string** ( v )
- **undefined** ( v )

#### Short-Circuiting

These methods return `onMissingValue` when they encounter a missing test value.

##### Core Methods

```js
valyzate.xxx() /* OR */ valyzate.not.xxx()
```
- **after** ( v, date = today ) - Comes after `date`?
- **array** ( v ) - Is an array?
- **before** ( v, date = today ) - Comes before `date`?
- **between** ( v, a, b ) - Is between `a` and `b` (inclusive)?
- **boolean** ( v ) - Is a boolean value?
- **configurable** ( v, prop ) - Can the object property be configured or deleted?
- **contains** ( v, a ) - Sequence contains `a` (string/array)?
- **cube** ( v ) - Is the cube root an integer?
- **date** ( v ) - Is a valid date?
- **dawn** ( v ) - Date falls in the (average) dawn hour (6)?
- **discrete** ( v ) - Does adding 1 to value change it?
- **divisibleBy** ( v, num ) - Is divisible by `num`?
- **dusk** ( v ) - Date falls in the (average) dusk hour (18)?
- **email** ( v ) - Is a valid email address?
- **empty** ( v ) - Has length of zero?
- **endsWith** ( v, a ) - Sequence ends with `a` (string/array)?
- **enumerable** ( v, prop ) - Is the object property enumerable?
- **equal** ( v, any ) - Is equal to (`===`)?
- **even** ( v ) - Is an even number?
- **extensible** ( v ) - Is the object extensible?
- **false** ( v ) - Is equal to `false`?
- **finite** ( v ) - Is a finite number?
- **float** ( v ) - Is a floating point number?
- **frozen** ( v ) - Is object frozen?
- **func** ( v ) - Is a function?
- **hasKey** ( v, str ) - Has the property (in)?
- **hasOwn** ( v, str ) - Directly has (own) property?
- **hour** ( v, num ) - Date falls in hour (0-23)?
- **infinity** ( v ) - Is `Infinity`?
- **integer** ( v ) - Is a whole number?
- **ipAddress** ( v ) - Is an IPv4 address (xxx.xxx.xxx.xxx)?
- **ipOctet** ( v ) - Is an IPv4 octet (0-255)?
- **itemIn** ( v, array ) - Is included by `array`?
- **json** ( v ) - Can be parsed as JSON?
- **length** ( v, num ) - Is of length `num`?
- **less** ( v, any ) - Is less than?
- **like** ( v, any ) - Is similar to (`==`)?
- **longer** ( v, num ) - Is length greater than `num`?
- **match** ( v, re, flgs ) - Matched by `re` pattern?
- **midnight** ( v ) - Date falls in the midnight hour (0)?
- **minute** ( v, num ) - Date falls in minute (0-60)?
- **month** ( v, num ) - Date falls in month (0-11)?
- **monthDay** ( v, num ) - Date falls in day of month (1-31)?
- **more** ( v, any ) - Is more than?
- **nan** ( v ) - Is not a number?
- **negative** ( v ) - Is value less than 0?
- **noon** ( v ) - Date falls in the midday hour (12)?
- **number** ( v ) - Is a number?
- **object** ( v ) - Is a non-null object?
- **odd** ( v ) - Is an odd number?
- **pattern** ( v ) - Is a valid regular expression pattern?
- **positive** ( v ) - Is value greater than 0?
- **quad** ( v ) - Is the quad root an integer?
- **regexp** ( v ) - Is a regular expression object?
- **reversible** ( v ) - Is string the same when reversed?
- **sealed** ( v ) - Is object sealed?
- **second** ( v, num ) - Date falls in second (0-60)?
- **shorter** ( v, num ) - Is length smaller than `num`?
- **square** ( v ) - Is the square root an integer?
- **startsWith** ( v, a ) - Sequence ends with `a` (string/array)?
- **string** ( v ) - Is a string?
- **thenable** ( v ) - Has a `then` property that is a function?
- **trimable** ( v ) - Has whitespace at beginning or end?
- **true** ( v ) - Is equal to `true`?
- **url** ( v ) - Is a valid URL?
- **weekDay** ( v, num ) - Date falls in day of week (0-6)?
- **writable** ( v, prop ) - Can the object property value be changed?
- **year** ( v, num ) - Date falls within year?
- **zero** ( v ) - Is equal to `0`?

##### String Inclusion/Composition Methods

These methods test if a string value is composed of or includes a given set of characters.

Each of these methods can be passed an optional `count` parameter.
- if `count` is undefined or null, test value must be composed of only the implied character(s).
- if `count` is >= 0, test value must contain implied character(s) exactly `count` times.
- otherwise (`count` is < 0), test value must contain implied character(s) at least `count` (absolute value) times.

All of these methods are ASCII character based.
```js
valyzate.xxx() /* OR */ valyzate.not.xxx()
```
- **alpha** ( v, count ) - Has letters?
- **alphanumeric** ( v, count ) - Has letters or numbers?
- **binary** ( v, count ) - Has 1's or 0's?
- **decimal** ( v, count ) - Has numbers 0 through 9 (same as .numeric())?
- **hexadecimal** ( v, count ) - Has numbers or letters A through F?
- **lowercase** ( v, count ) - Has lower case characters?
- **numeric** ( v, count ) - Has numbers 0 through 9 (same as .decimal())?
- **octal** ( v, count ) - Has numbers 0 through 7?
- **printable** ( v, count ) - Has printable characters?
- **punctuation** ( v, count ) - Has special characters?
- **ternary** ( v, count ) - Has 2's, 1's or 0's?
- **uppercase** ( v, count ) - Has upper case characters?
- **whitespace** ( v, count ) - Has whitespace characters?
- **word** ( v, count ) - Has word characters (letters, numbers, underscores)?

##### Day of Week Methods

These methods test if a date falls on a specific day of the week.
```js
valyzate.xxx() /* OR */ valyzate.not.xxx()
```
- **sunday** ( v )
- **monday** ( v )
- **tuesday** ( v )
- **wednesday** ( v )
- **thursday** ( v )
- **friday** ( v )
- **saturday** ( v )

##### Month of Year Methods

These methods test if a date falls in a specific month.
```js
valyzate.xxx() /* OR */ valyzate.not.xxx()
```
- **january** ( v )
- **february** ( v )
- **march** ( v )
- **april** ( v )
- **may** ( v )
- **june** ( v )
- **july** ( v )
- **august** ( v )
- **september** ( v )
- **october** ( v )
- **november** ( v )
- **december** ( v )

#### Extension

These methods can be added through the `extend` config parameter. They are applied to class prototypes to make them available on all validator instances.

##### Includer Methods

These methods check if test value is included in a list of values and are short-circuited for missing values.
```js
valyzate.in.xxx() /* OR */ valyzate.not.in.xxx()
```
These methods are derived from arrays or strings found in the `extend` object passed to configuration.
```js
alyze.config({ extend: { state: { codes: [ 'AL', 'CA', 'OR', 'NV' ] } } })
```
The above makes the following method available:
```js
valyzate.in.stateCodes(v)
```

##### Matcher Methods

These methods check if test value matches a regular expression pattern and are short-circuited for missing values.
```js
valyzate.matches.xxx() /* OR */ valyzate.not.matches.xxx()
```
These methods are derived from `RegExp` items found in the `extend` object passed to configuration.
```js
alyze.config({ extend: { usPhone: /^\(\d{3}\) \d{3}-\d{4}$/ } })
```
The above makes the following method available:
```js
valyzate.matches.usPhone(v)
```

##### Custom Methods

These methods perform a custom check on the test value and are short-circuited for missing values.
```js
valyzate.m.xxx() /* OR */ valyzate.not.m.xxx()
```
These methods are derived from functions found in the `extend` object passed to configuration.
```js
alyze.config({ extend: { verify: { isbn: function(v) { ... } } } })
```
Custom methods should return a boolean value, and they may also have additional parameters.

The above makes the following method available:
```js
valyzate.m.verifyIsbn(v)
```

#### List Element Validators

These methods deal with test values that are arrays and test every value in the array.

Note that:
- All validation methods (including _Extension_ methods) have versions that test array elements.
- Short-circuiting and negation rules apply in the same manner as a method's non-array counterpart.
- For missing array values in short-circuited methods a result of `false` is assumed for that value.
- Each method returns the same result as the non-array version if passed a non-array value.

##### 'All' Methods

These methods return true if every value in the test array passes the given test.
```js
valyzate.all.xxx()
/* OR */
valyzate.not.all.xxx()
```
Other method groups on the validator object also have `all` versions.

For instance, to use _Type Of_ methods, do
```js
valyzate.all.type.xxx()
/* OR */
valyzate.not.all.type.xxx()
```
A short-circuited `all` method will return `false` if any array value is missing.

##### 'Any' Methods

These methods return true if at least one value in the test array passes the given test.
```js
valyzate.any.xxx()
/* OR */
valyzate.not.any.xxx()
```
Other method groups on the validator object also have `any` versions.

For instance, to use _Instance Of_ methods, do
```js
valyzate.any.instance.xxx()
/* OR */
valyzate.not.any.instance.xxx()
```
A short-circuited `any` method will skip over missing array values.

## What Else?

###### Links

{ [dist files](https://bitbucket.org/captison/alyze/src/master/dist) }
{ [updates](https://bitbucket.org/captison/alyze/src/master/CHANGELOG.md) }
{ [feedback](https://bitbucket.org/captison/alyze/issues) }
{ [license](https://bitbucket.org/captison/alyze/src/master/LICENSE) }
{ [versioning](http://semver.org/) }

Please be sure to check 'updates' link when upgrading to a new version.

###### Tests

```
npm test
```

###### Finally

Happy Validating!
