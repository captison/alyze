## Alyze Change Log

### In Development (TODOs)


### Releases

#### 0.0.7

- added .pattern() to test for regexp pattern
- changed .regexp() to test for regexp object
- added .ipAddress() and .ipOctet()
- re-added/updated methods to test for sequence inclusion (string/array)
    - .contains()
    - .endsWith()
    - .startsWith()

#### 0.0.6

- .match() now accepts string regular expressions and flags parameter added
- added .word() to test for word characters
- added .startsWith() and .endsWith() for string testing
- added .reversible() to check string equality when reversed

#### 0.0.5

- webpack is now the app bundler
- application entry points now unified with umd/webpack

#### 0.0.4

- added .between()
- array element test methods added (all or any)
- .named() methods removed
- .contains() removed in favor of any.equal()
- extension methods (.ccNumber(), .daysAbbr(), etc.) no longer part of release but are available at test/extensions

#### 0.0.3

- added .undefined()
- added/updated inclusion/composition methods
    - .alpha()
    - .alphanumeric()
    - .binary()
    - .decimal()
    - .hexadecimal()
    - .lowercase()
    - .numeric()
    - .octal()
    - .printable()
    - .punctuation()
    - .ternary()
    - .uppercase()
    - .whitespace()
- added .trimable()
- added .discrete()
- added object metadata methods
    - .configurable()
    - .enumerable()
    - .extensible()
    - .writable()
- added .contains()
- .after() and .before() date parameter now defaults to today


#### 0.0.2

- added .empty() validation
- added .positive() and .negative() validations
- added .dawn() and .dusk() validations
- .config() now captures 'onMissingValue' and 'asMissing' settings as defaults for .create()

#### 0.0.1

- first version of **Alyze**!
