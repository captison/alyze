var path = require('path');
var webpack = require('webpack');
var packson = require('./package.json');


var moduleDir = __dirname;
var sourceDir = path.join(moduleDir, 'src');

module.exports = function(env)
{
    var config = {};

    config.entry = sourceDir + '/index.ts';

    config.output =
    {
        path: path.join(moduleDir, 'dist'),
        libraryTarget: 'umd',
        library: 'alyze'
    };

    config.resolve = { extensions: ['.ts', '.js'] };

    var typescriptLoader = { test: /\.ts$/, include: [ sourceDir ], loader: 'ts-loader' };

    config.module = { loaders: [ typescriptLoader ] };

    var bannerPluginOptions =
    {
        // the banner as string, it will be wrapped in a comment
        banner: packson.name + ' v' + packson.version + ' @' + new Date().toUTCString(),
        // if true, banner will not be wrapped in a comment
        raw: false,
        // if true, the banner will only be added to the entry chunks
        entryOnly: true
    };

    config.plugins =
    [
        new webpack.BannerPlugin(bannerPluginOptions)
    ];

    return config;
}
